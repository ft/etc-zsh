# -*- cperl -*-

#set_opt 'debug', 'true';
#set_opt 'verbose', 'true';
#set_opt 'dryrun', 'true';

register '--symlink', 'zshrc';
register '--symlink', 'zshenv';
register '--symlink', 'zlogout';
register '--symlink', 'dircolors';

register { glob        => 'functions/[_a-zA-Z0-9]*[a-z0-9]',
           method      => 'symlink',
           destination => '~/.zfunctions' };

sub zsh_byte_compile {
    my (@cmd, $old_dir);

    print qq{Byte-compiling shell code...\n};
    $old_dir = getcwd();
    push @cmd, q{zsh}, q{-fc};
    push @cmd, q{for i in .zshrc; do
                     [[ $i.zwc -nt $i ]] && continue
                     printf '  ..compiling %s\n' $i
                     zcompile -R -U $i
                 done
                 setopt extended_glob
                 cd .zfunctions &&
                 for i in **/*~*(.zwc|~)(-.); do
                     [[ $i.zwc -nt $i ]] && continue
                     printf '  ..compiling functions/%s\n' $i
                     zcompile -R -U $i
                 done};
    chdir($ENV{HOME}) or early_exit "Couldn't chdir($ENV{HOME}): $!";
    system(@cmd);
    chdir $old_dir or early_exit "Couldn't chdir($old_dir): $!";
}

sub zsh_upgrade_setup {
    my $old_dir = $ENV{HOME} . "/.zshrc.d";
    if (-d $old_dir) {
        print qq{Removing old split setup directory: $old_dir\n};
        system( (q{rm}, q{-rf}, $old_dir) );
    }
    foreach my $name ( qw{ real real.zwc pre3 pre3.zwc }) {
        my $file = $ENV{HOME} . "/.zshrc.$name";
        if (-e $file || -l $file) {
            print qq{Removing old setup file: $file\n};
            unlink $file;
        }
    }
}

add_hook { type  => 'perl',
           event => 'pre-deploy',
           code  => \&zsh_upgrade_setup };

add_hook { type => 'perl',
           event => 'post-deploy',
           code => \&zsh_byte_compile };

end
