#!/bin/zsh

emulate zsh
setopt null_glob
setopt extended_glob

fpath=( $PWD/functions "${fpath[@]}" )
() { (( $# > 0 )) } $^fpath/jump-to-delim(N) || {
    printf 'Could not find jump-to-delim in $fpath...\n'
    exit 1; }

source ./tests/test-library || {
    printf 'Failed to load test-library...\n'
    exit 1; }

zpty_run 'autoload -Uz jump-to-delim
          function jump-to-next-/ () { jump-to-delim / }
          function jump-to-prev-/ () { jump-to-delim / 0 }
          function jump-onto-next-/ () { jump-to-delim / 1 1 }
          function jump-onto-prev-/ () { jump-to-delim / 0 1 }
          function jump-across-next-/ () { jump-to-delim / 1 2 }
          function jump-across-prev-/ () { jump-to-delim / 0 2 }
          function jump-to-next-// () { jump-to-delim // }
          function jump-to-prev-// () { jump-to-delim // 0 }
          function jump-onto-next-// () { jump-to-delim // 1 1 }
          function jump-onto-prev-// () { jump-to-delim // 0 1 }
          function jump-across-next-// () { jump-to-delim // 1 2 }
          function jump-across-prev-// () { jump-to-delim // 0 2 }
          zle -N jump-to-next-/
          zle -N jump-to-prev-/
          zle -N jump-onto-next-/
          zle -N jump-onto-prev-/
          zle -N jump-across-next-/
          zle -N jump-across-prev-/
          zle -N jump-to-next-//
          zle -N jump-to-prev-//
          zle -N jump-onto-next-//
          zle -N jump-onto-prev-//
          zle -N jump-across-next-//
          zle -N jump-across-prev-//'


zpty_run 'bindkey "^xb" jump-to-prev-/
          bindkey "^xf" jump-to-next-/'

zle-test-define 'No delimiter in buffer'
zletest $'foo.bar.baz.zsh.scm\C-e\C-xb' | zle-results CURSOR=19

zle-test-define 'One delimiter directly to the left, going left'
zletest $'foo/\C-e\C-xb' | zle-results CURSOR=4

zle-test-define 'One level back to slash'
zletest $'foo/bar/baz/zsh.scm\C-e\C-xb' | zle-results CURSOR=12

zle-test-define 'One level forward to slash'
zletest $'foo/bar/baz/zsh.scm\C-a\C-xf' | zle-results CURSOR=2

zle-test-define 'Two levels back to slash'
zletest $'foo/bar/baz/zsh.scm\C-e\C-xb\C-xb' | zle-results CURSOR=8

zle-test-define 'Two levels forward to slash'
zletest $'foo/bar/baz/zsh.scm\C-a\C-xf\C-xf' | zle-results CURSOR=6

zle-test-define 'Three levels back to slash'
zletest $'foo/bar/baz/zsh.scm\C-e\C-xb\C-xb\C-xb' | zle-results CURSOR=4

zle-test-define 'Three levels forward to slash'
zletest $'foo/bar/baz/zsh.scm\C-a\C-xf\C-xf\C-xf' | zle-results CURSOR=10

zpty_run 'bindkey -r "^xb"
          bindkey -r "^xf"'


zpty_run 'bindkey "^xb" jump-onto-prev-/
          bindkey "^xf" jump-onto-next-/'

zle-test-define 'No delimiter in buffer (onto slash)'
zletest $'foo.bar.baz.zsh.scm\C-e\C-xb' | zle-results CURSOR=19

zle-test-define 'One delimiter directly to the left, going left (onto slash)'
zletest $'foo/\C-e\C-xb' | zle-results CURSOR=3

zle-test-define 'One level back onto slash'
zletest $'foo/bar/baz/zsh.scm\C-e\C-xb' | zle-results CURSOR=11

zle-test-define 'One level forward onto slash'
zletest $'foo/bar/baz/zsh.scm\C-a\C-xf' | zle-results CURSOR=3

zle-test-define 'Two levels back onto slash'
zletest $'foo/bar/baz/zsh.scm\C-e\C-xb\C-xb' | zle-results CURSOR=7

zle-test-define 'Two levels forward onto slash'
zletest $'foo/bar/baz/zsh.scm\C-a\C-xf\C-xf' | zle-results CURSOR=7

zle-test-define 'Three levels back onto slash'
zletest $'foo/bar/baz/zsh.scm\C-e\C-xb\C-xb\C-xb' | zle-results CURSOR=3

zle-test-define 'Three levels forward onto slash'
zletest $'foo/bar/baz/zsh.scm\C-a\C-xf\C-xf\C-xf' | zle-results CURSOR=11

zpty_run 'bindkey -r "^xb"
          bindkey -r "^xf"'


zpty_run 'bindkey "^xb" jump-across-prev-/
          bindkey "^xf" jump-across-next-/'

zle-test-define 'One delimiter directly to the left, going left (across slash)'
zletest $'foo/\C-e\C-xb' | zle-results CURSOR=2

zle-test-define 'One level back across slash'
zletest $'foo/bar/baz/zsh.scm\C-e\C-xb' | zle-results CURSOR=10

zle-test-define 'One level forward across slash'
zletest $'foo/bar/baz/zsh.scm\C-a\C-xf' | zle-results CURSOR=4

zle-test-define 'Two levels back across slash'
zletest $'foo/bar/baz/zsh.scm\C-e\C-xb\C-xb' | zle-results CURSOR=6

zle-test-define 'Two levels forward across slash'
zletest $'foo/bar/baz/zsh.scm\C-a\C-xf\C-xf' | zle-results CURSOR=8

zle-test-define 'Three levels back across slash'
zletest $'foo/bar/baz/zsh.scm\C-e\C-xb\C-xb\C-xb' | zle-results CURSOR=2

zle-test-define 'Three levels forward across slash'
zletest $'foo/bar/baz/zsh.scm\C-a\C-xf\C-xf\C-xf' | zle-results CURSOR=12

zpty_run 'bindkey -r "^xb"
          bindkey -r "^xf"'


zpty_run 'bindkey "^xb" jump-to-prev-//
          bindkey "^xf" jump-to-next-//'

zle-test-define 'One level back to slashslash'
zletest $'foo//bar//baz//zsh.scm\C-e\C-xb' | zle-results CURSOR=15

zle-test-define 'One level forward to slashslash'
zletest $'foo//bar//baz//zsh.scm\C-a\C-xf' | zle-results CURSOR=2

zle-test-define 'Two levels back to slashslash'
zletest $'foo//bar//baz//zsh.scm\C-e\C-xb\C-xb' | zle-results CURSOR=10

zle-test-define 'Two levels forward to slashslash'
zletest $'foo//bar//baz//zsh.scm\C-a\C-xf\C-xf' | zle-results CURSOR=7

zle-test-define 'Three levels back to slashslash'
zletest $'foo//bar//baz//zsh.scm\C-e\C-xb\C-xb\C-xb' | zle-results CURSOR=5

zle-test-define 'Three levels forward to slashslash'
zletest $'foo//bar//baz//zsh.scm\C-a\C-xf\C-xf\C-xf' | zle-results CURSOR=12

zpty_run 'bindkey -r "^xb"
          bindkey -r "^xf"'


zpty_run 'bindkey "^xb" jump-onto-prev-//
          bindkey "^xf" jump-onto-next-//'

zle-test-define 'One level back onto slashslash'
zletest $'foo//bar//baz//zsh.scm\C-e\C-xb' | zle-results CURSOR=13

zle-test-define 'One level forward onto slashslash'
zletest $'foo//bar//baz//zsh.scm\C-a\C-xf' | zle-results CURSOR=3

zle-test-define 'Two levels back onto slashslash'
zletest $'foo//bar//baz//zsh.scm\C-e\C-xb\C-xb' | zle-results CURSOR=8

zle-test-define 'Two levels forward onto slashslash'
zletest $'foo//bar//baz//zsh.scm\C-a\C-xf\C-xf' | zle-results CURSOR=8

zle-test-define 'Three levels back onto slashslash'
zletest $'foo//bar//baz//zsh.scm\C-e\C-xb\C-xb\C-xb' | zle-results CURSOR=3

zle-test-define 'Three levels forward onto slashslash'
zletest $'foo//bar//baz//zsh.scm\C-a\C-xf\C-xf\C-xf' | zle-results CURSOR=13

zpty_run 'bindkey -r "^xb"
          bindkey -r "^xf"'


zpty_run 'bindkey "^xb" jump-across-prev-//
          bindkey "^xf" jump-across-next-//'

zle-test-define 'One level back across slashslash'
zletest $'foo//bar//baz//zsh.scm\C-e\C-xb' | zle-results CURSOR=12

zle-test-define 'One level forward across slashslash'
zletest $'foo//bar//baz//zsh.scm\C-a\C-xf' | zle-results CURSOR=5

zle-test-define 'Two levels back across slashslash'
zletest $'foo//bar//baz//zsh.scm\C-e\C-xb\C-xb' | zle-results CURSOR=7

zle-test-define 'Two levels forward across slashslash'
zletest $'foo//bar//baz//zsh.scm\C-a\C-xf\C-xf' | zle-results CURSOR=10

zle-test-define 'Three levels back across slashslash'
zletest $'foo//bar//baz//zsh.scm\C-e\C-xb\C-xb\C-xb' | zle-results CURSOR=2

zle-test-define 'Three levels forward across slashslash'
zletest $'foo//bar//baz//zsh.scm\C-a\C-xf\C-xf\C-xf' | zle-results CURSOR=15

zpty_run 'bindkey -r "^xb"
          bindkey -r "^xf"'


zle-test-finish
