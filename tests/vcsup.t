#!/bin/zsh

emulate zsh
setopt null_glob
setopt extended_glob

# Load test subject
fpath=( $PWD/functions "${fpath[@]}" )
() { (( $# > 0 )) } $^fpath/vcsupinit(N) || {
    printf 'Could not find vcsupinit in $fpath...\n'
    exit 1; }

autoload -Uz vcs_info
vcs_info
autoload -Uz vcsupinit
vcsupinit ,
# Simulate updating vcs_info state in precmd() or similar:
function comma () {
    local -i rc
    , "$@"
    rc=$?
    vcs_info
    return $rc
}
function tool () {
    local -i rc
    vcsuptool "$@"
    rc=$?
    vcs_info
    return $rc
}

# Setup
suite=vcsup
base=$(mktemp -d "/tmp/ft-zshrc-test-$suite-XXXXXXXX")
test -d "$base" || exit 1
printf 'Test base directory: %s\n' $base
cd $base || { printf 'Could not enter test base directory: %s\n' $base
              exit 1; }
vcs_info

typeset -a prjdirs
prjdirs=( src/drivers/{uart,spi}
          src/periph/{usb,mu}
          doc/{design,api}
          examples/{cli,gui} )
mkdir -p "${prjdirs[@]}" || exit 1
for dir in "${prjdirs[@]}"; do
    printf 'README for %s...\n' $dir > $dir/README || exit 1
done
git init || exit 1
git add . || exit 1
git commit -m'Initial commit' || exit 1
vcs_info

# Tests

typeset -i ntests nskipped nfailed
function expect_stack () {
    local title="$1"
    local -i n rc
    shift
    (( ntests++ ))
    printf '#%s: %s\n' $ntests $title
    if (( ${#vcsupstack} != $# )); then
        printf '%s: Stack size does not match: %s != %s\n' \
               $0 ${#vcsupstack} $#
        (( nfailed++ ))
        vcsuptool list
        return 1
    fi
    for (( n=1; n < ( $# + 1 ); n++ )); do
        if [[ ${vcsupstack[$n]} != ${argv[$n]} ]]; then
            printf '%s: Stack entry (%s) does not match: %s != %s\n' \
                   $0 $n ${vcsupstack[$n]} ${argv[$n]}
            (( ( rc == 0 ) && nfailed++ ))
            (( rc = 1 ))
        fi
    done
    return $rc
}

function expect_pwd () {
    local title="$1"
    shift
    (( ntests++ ))
    printf '#%s: %s\n' $ntests $title
    if [[ $PWD != "$1" ]]; then
        printf '%s: Expected PWD not met: %s != %s\n' $0 $PWD $1
        (( nfailed++ ))
        return 1
    fi
    return 0
}

function expect_return_value () {
    local title="$1"
    local expect="$2"
    local actual="$3"
    (( ntests++ ))
    printf '#%s: %s\n' $ntests $title
    if [[ $expect != $actual ]]; then
        printf '%s: Expected return value (%s) not met: %s\n' \
               $0 $expect $actual
        (( nfailed++ ))
        return 1
    fi
    return 0
}

echo
# Default behaviour:
#
#   - Dups are not filtered
#   - Only argless "comma" pushes automatically when cd-ing to *basedir*

expect_stack "Empty stack is empty"

comma src/drivers/uart || exit 1
comma
expect_stack "One entry stack" $base/src/drivers/uart

comma -
# Default behaviour is stack-push-home == true, so:
expect_stack "Stack empty after pop"
expect_pwd "Back to src drivers/uart" $base/src/drivers/uart

# Try the same with stack-push-home == false:
zstyle ':vcsup:*' stack-push-home false
comma
tool clear
comma src/drivers/uart || exit 1
comma
expect_stack "Stack still empty"
expect_pwd "Currently in base dir" $base
zstyle ':vcsup:*' stack-push-home true

# Default behaviour does not push automatically:
comma src/drivers/uart || exit 1
comma ../spi || exit 1
tool push
comma ../../periph || exit 1
comma mu || exit 1
tool push
comma ../usb || exit 1
tool push

expect_stack "Many entries on the stack #1" \
             $base/src/periph/usb \
             $base/src/periph/mu \
             $base/src/drivers/spi

# comma needs to propagate builtin errors properly:
comma does-not-exist 2> /dev/null
expect_return_value "Comma fails with non-existing directory" 1 $?

# "comma ++" is a short-hand for "vcsuptool push"
cd $base
tool clear
comma src/drivers/spi
comma ++
expect_stack "comma ++ just pushed \$PWD" $PWD
oldpwd=$PWD
comma
expect_stack "Default behaviour does not filter dups" $oldpwd $oldpwd

# "comma +" pops bottom entry from stack:
cd $base
tool clear
comma src/drivers/uart || exit 1
tool push
comma ../spi || exit 1
tool push
comma ../../periph || exit 1
tool push
comma mu || exit 1
tool push
comma ../usb || exit 1
tool push

expect_stack "Many entries on the stack #2" \
             $base/src/periph/usb \
             $base/src/periph/mu \
             $base/src/periph \
             $base/src/drivers/spi \
             $base/src/drivers/uart

comma +

expect_stack "Many entries on the stack #3" \
             $base/src/periph/usb \
             $base/src/periph/mu \
             $base/src/periph \
             $base/src/drivers/spi

expect_pwd "Now in previous bottom entry from stack #1" $base/src/drivers/uart

# The same also works with no tool push calls if stack-auto-push is true
zstyle ':vcsup:*' stack-auto-push true
cd $base
tool clear
comma src/drivers/uart || exit 1
comma ../spi || exit 1
comma ../../periph || exit 1
comma mu || exit 1
comma ../usb || exit 1

expect_stack "Many entries on the stack #4" \
             $base/src/periph/usb \
             $base/src/periph/mu \
             $base/src/periph \
             $base/src/drivers/spi \
             $base/src/drivers/uart

comma +

expect_stack "Many entries on the stack #5" \
             $base/src/drivers/uart \
             $base/src/periph/usb \
             $base/src/periph/mu \
             $base/src/periph \
             $base/src/drivers/spi

expect_pwd "Now in previous bottom entry from stack #2" $base/src/drivers/uart
zstyle ':vcsup:*' stack-auto-push false

# The same also works with no tool push calls and *cd* if stack-auto-push-cd
# is true.
zstyle ':vcsup:*' stack-auto-push-cd true
cd $base
tool clear
cd src/drivers/uart || exit 1
cd ../spi || exit 1
cd ../../periph || exit 1
cd mu || exit 1
cd ../usb || exit 1

expect_stack "Many entries on the stack #6" \
             $base/src/periph/usb \
             $base/src/periph/mu \
             $base/src/periph \
             $base/src/drivers/spi \
             $base/src/drivers/uart

comma +

expect_stack "Many entries on the stack #7" \
             $base/src/periph/usb \
             $base/src/periph/mu \
             $base/src/periph \
             $base/src/drivers/spi

expect_pwd "Now in previous bottom entry from stack #3" $base/src/drivers/uart
zstyle ':vcsup:*' stack-auto-push-cd false

# Teardown
printf '\nRemoving test base directory: %s\n' $base
rm -Rf $base || exit 1

# Results
printf '\nResults: %s test (%s succeeded, %s failed, %s skipped)\n' \
       $ntests $(( ntests - nfailed )) $nfailed $nskipped
