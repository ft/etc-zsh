# -*- shell-script -*-

### ft's z-shell setup #######################################################

# This setup used to try to support all publically available zsh versions, even
# ancient ones like 2.x. That required a lot of workarounds and kills startup
# time a bit, since versions had to be checked in a *lot* of places.
#
# That goal is now gone (there is a "full-version-support" branch in the git
# repository that is branched off at a point, where this was still the case).
#
# The baseline for a minimum version number for the setup to support is the
# version included in Debian's "oldstable" release. At the time of writing
# (2015-06-24) that is "squeeze" which included v4.3.17 of zsh.
#
# So there you go. That's now the required minimum.


### Profiling ################################################################

if [[ -n $ZSHRC_PROFILE ]] && [[ $ZSHRC_PROFILE != 0 ]]; then
    ZSHRC_PROFILE=1
    zmodload zsh/zprof
fi


### Environment variables ####################################################

export BROWSER="w3m"
export EDITOR='ed'
export PAGER="less"
export TMPDIR="${HOME}/tmp"
export VISUAL='vim'

if [[ -n ${ZSH_TRUST_ME_ON_THIS_CTYPE} ]] ; then
    export LC_CTYPE=${ZSH_TRUST_ME_ON_THIS_CTYPE}
fi

# Default GTK is a tad bright.
export GTK_THEME=Adwaita:dark

# GNU feels like fucking with the default output of ls. Not cool.
export QUOTING_STYLE='literal'

export MANWIDTH=80

export SLRNHELP="${HOME}/etc/slrn/slrnhelp"

export DEBFULLNAME='Frank Terbeck'
export DEBEMAIL='ft@bewatermyfriend.org'
export GRMLEMAIL='ft@grml.org'

export MENUCONFIG_COLOR=mono

export LYNX_CFG=~/.lynx/config

TMPDIR="${HOME}/tmp"
if [[ ! -d ${TMPDIR} ]]; then
    TMPDIR=/tmp
fi
export TMPDIR

if [[ -z "$GUIX_ENVIRONMENT" ]]; then
    path=( ${HOME}/bin
           ${HOME}/.idris2/bin
           ${HOME}/.ghcup/bin
           ${HOME}/.cabal/bin
           ${HOME}/.local/bin
           ${HOME}/.cargo/bin
           /usr/local/{,s}bin
           /{,s}bin
           /usr/{,s}bin
           "${path[@]}" )

    for i in /opt/*/bin(N/) /usr/games ; do
        [[ -d $i ]] && path=( $i "${path[@]}" )
    done; unset i

    typeset -U path
fi

### Z-Shell specific parameters ##############################################

### If the execution of a command takes longer than
### REPORTTIME (in seconds), time statistics are printed
REPORTTIME=4

HISTSIZE=9999
HISTFILE="${HOME}/.zhistory"
SAVEHIST=6000

DIRSTACKSIZE=30

NULLCMD="cat"
READNULLCMD=${PAGER:-more}

fpath=( ~/.zfunctions
        ~/.zfunctions/**/*(/N)
        "${fpath[@]}" )

# On Debian, packages put additional completion in this directory:
test -d /usr/share/zsh/vendor-completions &&
    fpath+=( /usr/share/zsh/vendor-completions )

typeset -U fpath


### Setup specific global parameters #########################################

# ZSHRC_VERBOSE:
#    0:   no verbosity (only errors)
#   >0:   print files that are loaded by zrcsource
#         warns when a file needs to be (re)compiled
#   >1:   print functions zrcautoload cannot find
#   >2:   print functions zrcautoload loads
#         makes zrcneedcomp verbose
#   >3:   make version checking verbose
ZSHRC_VERBOSE=${ZSHRC_VERBOSE:-0}

typeset -ga zle_init_functions
typeset -ga zle_finish_functions
typeset -ga zle_keymap_functions
typeset -gA ft_zle_state
typeset -A key
typeset -a hosts

hosts=( ftp.de.debian.org
        ftp.kernel.org
        ftp.ccc.de
        sunsite.rwth-aachen.de
        ftp.{free,net,open}bsd.org
        {www.,}bewatermyfriend.org
        {www.,}{fh,rwth}-aachen.de
        finger.kernel.org )

# hosts where I normally got sudo privileges.
# format: comma-seperated-user-list@hostname
sudo_hosts=( magnum,hawk,ft@fhm hawk@{fsst,bones,jim,spock} magnum@earthbound )


### Utility functions ########################################################

# Usage: zsh-version OPERATOR MAJOR [MINOR] [PATCHLEVEL]
#
# Operator is either = or >= (note that the latter obviously needs to be
# quoted).

# Precompute the parts of the version string. This saves lots of time, since
# the version checks are run *very* often in this setup.
typeset FT_ZSH_VERSION_MAJOR FT_ZSH_VERSION_MINOR FT_ZSH_VERSION_PATCHLEVEL
typeset -a tmp
tmp=( ${(s:.:)ZSH_VERSION} )
FT_ZSH_VERSION_MAJOR=${tmp[1]}
FT_ZSH_VERSION_MINOR=${tmp[2]}
FT_ZSH_VERSION_PATCHLEVEL=${${tmp[3]}%%-*}
unset tmp

function zsh-version() {
    local -i maj=$2 min=$3 plv=$4
    if [[ $1 == '>=' ]]; then
        (( FT_ZSH_VERSION_MAJOR < maj )) && return 1
        (( FT_ZSH_VERSION_MAJOR > maj )) && return 0
        if (( FT_ZSH_VERSION_MAJOR == maj )); then
            (( FT_ZSH_VERSION_MINOR < min )) && return 1
            (( FT_ZSH_VERSION_MINOR > min )) && return 0
            if (( FT_ZSH_VERSION_MINOR == min )); then
                (( FT_ZSH_VERSION_PATCHLEVEL < plv )) && return 1
                (( FT_ZSH_VERSION_PATCHLEVEL >= plv )) && return 0
            fi
        fi
    else
        (( FT_ZSH_VERSION_MAJOR == maj )) &&
            (( FT_ZSH_VERSION_MINOR == min )) &&
            (( FT_ZSH_VERSION_PATCHLEVEL == plv )) &&
            return 0
    fi
}

function zprintf() {
    local -i level; local format
    level=$1 ; format=$2; shift; shift
    (( ZSHRC_VERBOSE < level )) && return 0
    printf '[%2s] '${format} ${level} "$@"
    return $?
}

zprintf 1 'Starting zsh v%s\n' ${ZSH_VERSION}

function source_if_exists() {
    local rc="${HOME}/.$1"
    if [[ -r "$rc" ]]; then
        zprintf 1 "zshrc: loading %s\n" "$rc"
        source "$rc"
    fi
    return 0
}

#### check_com(): check if a command exists
### eg: check_com "vim -p"
function check_com() {
    #setopt localoptions xtrace
    local words
    local -i comonly
    local cmd

    if [[ $1 == '-c' ]] ; then
        (( comonly = 1 ))
        shift
    else
        (( comonly = 0 ))
    fi

    if (( ${#argv} != 1 )) ; then
        printf 'usage: check_com [-c] <command>\n' >&2
        return 1
    fi

    words=(${(z)1})
    cmd=${words[1]}

    if (( comonly > 0 )) ; then
        [[ -n ${commands[$cmd]}  ]] && return 0
        return 1
    fi

    if   [[ -n ${commands[$cmd]}    ]] \
      || [[ -n ${functions[$cmd]}   ]] \
      || [[ -n ${aliases[$cmd]}     ]] \
      || [[ -n ${reswords[(r)$cmd]} ]] ; then

        return 0
    fi

    return 1
}

### check_com_print(): check_com() + error msg
function check_com_print() {
    local command

    if [[ $1 == '-c' ]]; then
        command=$2
    else
        command=$1
    fi
    if ! check_com "$@" ; then
        printf '%s not found in $path.\n' ${command}
        return 1
    fi
    return 0
}

### salias(): smart creation of sudo aliases
### This is based on the salias() function I wrote for the grml zshrc.
### However, I changed it again to work with older versions of zsh as well.
### Added -c option. Added -C option.
### Note, that this has _nothing_ to do with suffix aliases.
function salias() {
    local only=0 multi=0 check=0 chkcom=1 sudo_host=0
    local sudo_u sudo_us sudo_h i j key val mval
    if (( ${#argv} == 0 )) ; then
        printf 'salias(): Missing argument. Try salias -h for help.\n'
        return 1
    fi
    while [[ $1 == -* ]] ; do
        case $1 in
            (-o) only=1  ;;
            (-C) chkcom=0 ;;
            (-c) check=1 ;;
            (-a) multi=1 ;;
            (--) shift ; break ;;
            (-h)
printf 'usage: salias [-h|-o|-a|-c|-C] <alias-expression>\n'
printf '  -h      shows this help text.\n'
printf '  -a      replace '\'' ; '\'' sequences with '\'' ; sudo '\''.\n'
printf '          be careful using this option.\n'
printf '  -o      only sets an alias if a preceding sudo would be needed.\n'
printf '  -c      check $sudo_hosts[] array.\n'
printf '  -C      don'\''t check if the command in the alias exists.\n'
printf '          Use this if the command requires a special check, that.\n'
printf '          check_com() cannot deal with.\n'
return 0
;;
            (*) printf "unkown option: '%s'\n" "$1" ; return 1 ;;
        esac
        shift
    done
    if (( check > 0 )) ; then
        for i in ${sudo_hosts} ; do
            sudo_u=${${(s:@:)i}[1]}
            sudo_us=(${(s:,:)sudo_u})
            sudo_h=${${(s:@:)i}[2]}
            if [[ ${sudo_h} == ${HOST} ]] ; then
                for j in ${sudo_us} ; do
                    if [[ ${USER:-${LOGNAME}} == $j ]] ; then
                        ### user and host matches, okay.
                        sudo_host=1
                        break;
                    fi
                done
                break;
            fi
        done
        (( sudo_host == 0 )) && return 0
    fi
    if (( ${#argv} > 1 )) ; then
        printf 'Too many arguments %s\n' "${#argv}"
        return 1
    fi
    ### create the alias
    key="${1%%\=*}" ;  val="${1#*\=}"
    if (( EUID == 0 )) && (( only == 0 )); then
        if (( chkcom > 0 )) ; then
            check_com ${val} || return 0
        fi
        alias -- "${key}=${val}"
    elif (( EUID > 0 )) ; then
        if (( multi > 0 )) ; then
            mval=(${(s, ; ,)val})
            if (( chkcom > 0 )) ; then
                check_com ${mval[1]} || return 0
            fi
            val="sudo ${mval[1]}"
            shift mval
            for i in ${mval} ; do
                if (( chkcom > 0 )) ; then
                    check_com $i || return 0
                fi
                val="${val} ; sudo $i"
            done
            alias -- "${key}=${val}"
        else
            if (( chkcom > 0 )) ; then
                check_com ${val} || return 0
            fi
            alias -- "${key}=sudo ${val}"
        fi
    fi
    return 0
}

### xalias(): check for executable, then create alias
### only supposed to be used with simple aliases.
function xalias() {
    local key val com
    if (( ${#argv} == 0 )) ; then
        printf 'xalias(): Missing argument.\n'
        return 1
    fi
    if (( ${#argv} > 1 )) ; then
        printf 'xalias(): Too many arguments %s\n' "${#argv}"
        return 1
    fi

    key="${1%%\=*}" ;  val="${1#*\=}"
    check_com ${val} && alias -- "${key}=${val}"
    return 0
}

### xhashd(): check for directory, then create hash -d
function xhashd() {
    local key val com
    if (( ${#argv} == 0 )) ; then
        printf 'xhashd(): Missing argument.\n'
        return 1
    fi
    if (( ${#argv} > 1 )) ; then
        printf 'xhashd(): Too many arguments %s\n' "${#argv}"
        return 1
    fi

    key="${1%%\=*}" ;  val="${1#*\=}"
    [[ -d ${val} ]] && hash -d -- "${key}=${val}"
    return 0
}

### zrcautoload(): a wrapper for autoload
function zrcautoload() {
    setopt local_options
    setopt extended_glob
    local fdir ffile
    local -i ffound
    ffile=$1
    (( ffound = 0 ))
    for fdir in ${fpath} ; do
        [[ -e ${fdir}/${ffile} ]] && (( ffound = 1 )) && break
    done
    if (( ffound == 0 )) ; then
        zprintf 2 "  zrcautoload: cannot find %s\n" ${ffile}
        return 1
    fi
    zprintf 3 "  zrcautoload: loading %s\n" ${ffile}
    autoload -U ${ffile} || return 1
    return 0
}

### bind2maps: wrapper for bindkey and $key[]
function bind2maps () {
    emulate -L zsh
    local i sequence widget
    local -a maps

    while [[ "$1" != "--" ]]; do
        maps+=( "$1" )
        shift
    done
    shift

    if [[ "$1" == "-s" ]]; then
        shift
        sequence="$1"
    else
        sequence=''
        for i in ${(s: :)1}; do
            if [[ -n "${key[$i]}" ]]; then
                sequence+="${key[$i]}"
            else
                sequence+="$i"
            fi
        done
    fi
    widget="$2"

    [[ -z "$sequence" ]] && return 1

    for i in "${maps[@]}"; do
        builtin bindkey -M "$i" "$sequence" "$widget"
    done
}

### zrcmodload(): wrapper for zmodload (tests ${module_path})
function zrcmodload() {
    local opts mod matches good

    opts=() ; (( good = 0 ))
    while [[ $1 == -* ]] ; do
        opts=( ${opts} $1 )
        shift
    done
    mod=$1
    shift

    if [[ ${modules[$mod]} == 'loaded' ]] ; then
        zprintf 3 "   zrcmodload: already loaded: %s - Skipping.\n" ${mod}
        return 0
    fi

    if [[ ${mod} == zsh/* ]] || [[ ${mod} == 'compctl' ]] ; then
        (( good = 1 ))
    fi

    if (( good == 0 )) ; then
        zprintf 0 'Failed to handle call: zrcmodload'
        zprintf 0 ' %s %s %s\n' "${opts}" "${mod}" "${(j: :)@}"
        return 3
    fi

    matches=( ${^module_path}/${mod}.(so|dll)(N) )
    (( ${#matches} == 0 )) && return 2

    zprintf 3 "   zrcmodload: loading %s\n" ${mod}
    zmodload "${opts[@]}" ${mod} "$@"
    return $?
}

# zrclistkeys() show a list of keysyms strings
function zrclistkeys() {
    local i
    for i in ${(kon)key}; do
        printf '%13s: '\''%s'\''\n' $i ${(V)${key[$i]}}
    done
}


### Early optional setup file ################################################

source_if_exists zshrc.pre


### Z-Shell Options ##########################################################

# +++ initialization options +++

### automagically export defined variables; not recommended
setopt no_all_export
### after /etc/zshenv, all rc files are sourced as supposed.
setopt rcs
### i don't care if global RCs are run; my config is fairly complete.
setopt no_global_rcs
### export will not be local to functions with this one; don't rely on it.
setopt no_global_export

# +++ {in,out}put options +++

### allow '>' to truncate, and '>>' to create files
setopt clobber
### try to correct the spelling of commands
setopt correct
### try to correct every word; this can be dangerous with some commands (e.g. mv)
setopt no_correct_all
### flowcontrol (usually) ^S ^Q
setopt no_flow_control
### hash executed commands
setopt hash_cmds
### alias for hash_cmds
#setopt hash_all
### alias for hash_cmds
#setopt track_all
### hash dirs of executed commands
setopt hash_dirs
### ignore ^D
setopt ignore_eof
### allow comments in interactive shells
setopt no_interactive_comments
### eg. '/usr/bin/' is in your $path, and there is a command
### '/usr/bin/X11/xinit' you can start it by 'X11/xinit'
setopt no_path_dirs
### if a command returns non-zero, tell the user i got a solution for this in
### my prompt, so i don't need it. using the format $'...', where a backslashed
### single quote can be used.
setopt no_print_exit_value
### do not query the user before execing 'rm *' or 'rm path/*'
setopt no_rm_star_silent
### allow short loop forms
setopt short_loops
### if a line ends with a backquote, and there are an odd number of backquotes
### on the line, ignore the trailing backquote. this is useful on some
### keyboards where the return key is too small, and the backquote key lies
### annoyingly close to it. _hehe_
setopt no_sun_keyboard_hack
### print warning if a mail file has been accessed since the shell last checked.
setopt no_mail_warning
### alias for mail_warning
#setopt mail_warn
### print eight bit characters literally in completion lists, etc. not
### needed if your system returns correct values
setopt no_print_eight_bit
### like 'rm_star_silent' but wait ten seconds instead of querying
setopt no_rm_star_wait
### 'correct{,_all}' expect errors that would happen on dvorak keyboards.
setopt no_dvorak
### expand aliases; you probably want this
setopt aliases

# +++ expansion+globbing options +++

### if a pattern or glob is badly formed, print out an error
setopt bad_pattern
### echo {abc.}file :: .file afile bfile cfile
setopt brace_ccl
### only report an error if _all_ globs for a command fail
setopt no_csh_nullglob
### the mighty =command expansion :)
setopt equals
### the force is strong with this one...
setopt extended_glob
### abso-freakin-lutely
setopt glob
### if set, allow 'name=*' and 'name=(*)'; since the latter is more clear, i'm
### unsetting this option
setopt no_glob_assign
### don't require a leading dot for matching "hidden" files
setopt no_glob_dots
### alias for glob_dots
#setopt dot_glob
### foo="*"; print $foo; will print all files; not for me, thanks
setopt no_glob_subst
### do not perform brace expansion;
setopt no_ignore_braces
### alias for no_ignore_braces
#setopt brace_expand
### do expansions on everything the looks like an 'name=express' assignment.
setopt no_magic_equal_subst
### append '/' to dirnames generated by globbing
setopt no_mark_dirs
### print an error if a glob didn't return a result
setopt nomatch
### is a glob does not return matches, remove the glob from the argumentlist
### instead of reporting an error
setopt no_nullglob
### sort filenames numerically rather than lexicographically, if possible.
setopt no_numeric_glob_sort
### array expansions of the form `foo${xx}bar', where the parameter 'xx' is set
### to (a b c), are substituted with `fooabar foobbar foocbar' instead of the
### default `fooa b cbar'.
setopt rc_expand_param
### make globbing more 'sh-like'; please, no!
setopt no_sh_glob
### treat unset parameters as if they were empty when substituting.
setopt unset
### treat trailing set of parentheses as a qualifier list
setopt bare_glob_qual
### a [\+\*@\?\!] that precedes a pair of parentheses affects globbing.
setopt no_ksh_glob
### make globbing case sensitive
setopt case_glob
### repect multibyte characters when found in strings
setopt multibyte
### make regular expression matches case sensitive
setopt case_match
### use pcre style regular expressions, if pcre is available
setopt rematch_pcre

# +++ history options +++

### when exiting, append history entries to $HISTFILE, rather
### than replacing the old file; this is the default
setopt append_history
### alias for append_history
#setopt hist_append
### enable '!' history expansion
setopt bang_hist
### alias for bang_hist
#setopt hist_expand
### additional info in $HISTFILE
setopt no_extended_history
### add '|' to output redirections in the history.
setopt no_hist_allow_clobber
### beep? beep yourself!eleven!!
setopt no_hist_beep
### don't add entered command to history, if it's a dup of the previous event.
setopt hist_ignore_dups
### if the commandline starts with a whitespace, don't add it to history
setopt hist_ignore_space
### dont add 'history' command (fc -l) to the history
setopt hist_no_store
### remove unneeded blanks from commands in history
setopt no_hist_reduce_blanks
### bullet-proof history-expansion
setopt no_hist_verify
### don't add functions to history
setopt no_hist_no_functions
### alias for no_hist_no_functions
#setopt log
### if the internal history needs to be trimmed, throw away dups first
setopt no_hist_expire_dups_first
### when using history don't find dups, even if they are not contiguous.
setopt hist_find_no_dups
### if the entered command is a dup, remove the old one from history.
setopt hist_ignore_all_dups
### don't write dups to $HISTFILE
setopt hist_save_no_dups
### append every single command to $HISTFILE immediately after hitting
### ENTER.
setopt no_inc_append_history
### always import new commands from $HISTFILE (see 'inc_append_history')
setopt no_share_history
### when saving history: DATA => $HISTFILE.new; mv $HISTFILE.new $HISTFILE
### you normally don't unset this one.
setopt hist_save_by_copy
### history substitutions (:s, :&) performed with patterns instead of
### strings
setopt no_hist_subst_pattern

# +++ completion options +++

### if zsh prints out a list (eg. on an ambiguous completion), it reuses the
### old prompt instead of printing a new one underneath the list; this breaks
### menucompletion if turned off
setopt always_last_prompt
### if a completion is performed with the cursor within a word, and a full
### completion is inserted, the cursor is moved to the end of the word; that
### is, the cursor is moved to the end of the word if either a single match is
### inserted or menu completion is performed; worthless if 'complete_in_word'
### is off
setopt no_always_to_end
### autom. list choices on ambiguous completion
setopt auto_list
### use menucompletion after the 2nd consecutive completion request;
### overwritten by 'menu_complete'
setopt auto_menu
### any parameter that is set to the absolute name of a directory immediately
### becomes a name for that directory; otherwise the parameter must be used is
### the ~parameter form.
setopt no_auto_name_dirs
### intelligently remove automatically inserted characters when completing
setopt no_auto_param_keys
### when completing a directory name add a slash instead of a space
setopt auto_param_slash
### intelligently remove the trailing slash from a completed directory
setopt auto_remove_slash
### don't expand aliases _before_ completion has finished
setopt complete_aliases
### if unset the cursor is set to the end of the word if completion is started
setopt complete_in_word
### complete as much of a completion until it gets ambiguous.
setopt list_ambiguous
### how about --- NO!
setopt no_list_beep
### identify filetype by a trailing marker in completion lists
setopt list_types
### always use menu completion
setopt no_menu_complete
### enable globbing in completions; cycle through globbing matches in a menu
### (do not insert them right away).
setopt no_glob_complete
### force entire path of a completion to be hashed
setopt hash_list_all
### in completion, recognize exact matches even if they are ambiguous.
setopt no_rec_exact
### automatically list choices when a completion function is called twice
### in succession
setopt no_bashautolist
### try to make completion lists smaller (but ugly)
setopt no_list_packed
### lay out matches in completion lists horizontally
setopt no_list_rows_first

# +++ shell emulation options +++

### make builtin 'echo' compatible to BSD's 'echo'
setopt no_bsd_echo
### i don't get this one :-/
setopt no_csh_junkie_history
### this enables some history shortcuts, that csh users might be used to
setopt no_csh_junkie_loops
### change the way "" and '' work to match csh
setopt no_csh_junkie_quotes
### emulate ksh array handling
setopt no_ksh_arrays
### changes the output of setopt without arguments
setopt no_ksh_option_print
### when set, the command 'builtin' can be used to execute shell builtins
setopt no_posix_builtins
### perform filename expansion (e.g., ~ expansion) before parameter expansion,
### command substitution, arithmetic expansion and brace expansion. if this
### option is unset, it is performed after brace expansion, so things like
### `~$USERNAME' and `~{pfalstad,rc}' will work.
setopt no_sh_file_expansion
### try to interpret single-letter options to set{,opt} like ksh would.
setopt no_sh_option_letters
### sh-style wordsplitting; not for me.
setopt no_sh_word_split
### emulate ksh function autoloading
setopt no_ksh_autoload
### use ':' instead of $NULLCMD and $READNULLCMD
setopt no_sh_nullcmd
### ignore $NULLCMD and $READNULLCMD; redirection without command will fail.
setopt no_csh_nullcmd
### alters the typeset commands to match ksh's behaviour
setopt no_ksh_typeset
### handle signals and run traps immediately
setopt no_traps_async
### only allow ASCII characters a-z, A-Z, 0-9 and _ in identifiers
setopt no_posix_identifiers
### use $BASH_REMATCH[] instead of the default $match[]
setopt no_bash_rematch
### if set, $foo[0] == $foo[1] with ksharrays unset
setopt no_ksh_zero_subscript

# +++ script+function options +++

### if set, execute ZERR trap and exit if a command returns non-zero
setopt no_err_exit
### yes, _do_ run the commands i enter.
setopt exec
### set $0 to the function/script name
setopt function_arg_zero
### shell options are to be restored after returning from a shell function
setopt local_options
### enable multiple redirections
setopt multios
### print shell input lines as they are read.
setopt no_verbose
### same as 'set -x'
#setopt no_xtrace
### similar to 'local_options', but works on traps
setopt no_local_traps
### print octal values the '0741' way
setopt octal_zeroes
### output hex-numbers in '0x1F' format (see 'octal_zeroes')
setopt c_bases
### similar to 'err_exit' but a 'return' would be executed rather than an
### 'exit'
setopt no_err_return
### if this is unset, executing any of the 'typeset' family of commands
### with no options and a list of parameters that have no values to be
### assigned but already exist will display the value of the parameter. if
### the option is set, they will only be shown when parameters are selected
### with the '-m' option. the option '-p' is available whether or not the
### option is set.
setopt no_typeset_silent
### linenumbers of expressions in 'eval' are tracked seperately
setopt eval_lineno
### print a warning when creating a global parameter by an assignment in a
### function
setopt warn_create_global
### run the DEBUG trap *before* each command
setopt debug_before_cmd

# +++ chdir options +++

### if a directoryname is entered like a command, and there is no command of
### that name; the 'cd' command is executed for that directory
setopt no_autocd
### make cd push the old directory to the dirstack
setopt auto_pushd
### if cd would fail, because the arg is not a dir, try to expand the argument
### as if it was called the ~expression way
setopt no_cdable_vars
### _just_don't_lie_to_me_
setopt chase_links
### alias for chase_links
#setopt physical
### don't push dups on the dirstack
setopt pushd_ignore_dups
### Exchanges the meanings of `+' and `-' when used with a number to specify a
### directory in the stack.
setopt pushd_minus
### do not print the dirstack after popd/pushd
setopt no_pushd_silent
### make 'pushd' with no argument, act like 'pushd ${HOME}'
setopt no_pushd_to_home
### _just_don't_lie_to_me_
setopt chase_dots

# +++ job control options +++

### if you've got a simple command suspened, say 'mutt', and you forgot that
### you have already got a mutt running and try to start another mutt, the old
### running mutt is resumed, rather than starting a new process
setopt no_auto_resume
### run background jobs at lower priority
setopt no_bg_nice
### send SIGHUP to background processes on exit.
setopt no_hup
### list jobs in the long format by default
setopt long_list_jobs
### allow job control; _absolutely_yes_
setopt monitor
### report status of background jobs immediately
setopt notify
### report status of bg-jobs if exiting a shell with job control enabled
setopt check_jobs
### if set, stopped jobs are disowned and restarted immediately; i don't
### see, why you would want this.
setopt no_auto_continue

# +++ prompt options +++

### print '\r' before printing the prompt.
setopt no_prompt_cr
### turn on various expansions in prompts
setopt prompt_subst
### alias for prompt_subst
#setopt prompt_vars
### if set, '!' is treated specially in prompt expansion.
setopt no_prompt_bang
### if set, '%' is treated specially in prompt expansion.
setopt prompt_percent
### remove any right prompt from display when accepting a command line.
### this may be useful with terminals with other cut/paste methods.
setopt transient_rprompt
### attempt to preserve partial lines (no effect if 'prompt_cr' is unset).
setopt no_prompt_sp

# +++ line editor options +++

### beep on error in zle; hell _no_!
setopt no_beep
### start zle in overstrike mode
setopt no_overstrike
### switch of multiline editing; who would want this?
setopt no_single_line_zle
### use the zsh-line-editor
setopt zle
### bindkey -v equivalent
setopt no_vi
### bindkey -e equivalent
setopt emacs

# +++ state options +++

### turn on privileged mode.
setopt no_privileged
### allow the character sequence _''_ to signify a single quote within singly
### quoted strings. note this does not apply in quoted strings
setopt no_rc_quotes
### turn on restricted mode.
setopt no_restricted
### make the shell exit after the first command from stdin
setopt no_single_command
### alias for for single_command
#setopt one_cmd

# +++ Other options +++

### the following options are just included for completeness you don't have to
### set these, they are normally set to the right value automatically. set
### state to "login shell"
#setopt no_login

### commands are being read from the standard input. commands are read from
### standard input if no command is specified with '-c' and no file of commands
### is specified. if SHIN_STDIN is set explicitly on the command line, any
### argument that would otherwise have been taken as a file to run will instead
### be treated as a normal positional parameter. note that setting or unsetting
### this option on the command line does not necessarily affect the state the
### option will have while the shell is running - that is purely an indicator
### of whether on not commands are actually being read from standard input. the
### value of this option cannot be changed anywhere other than the command
### line.
#setopt shin_stdin

### alias for shin_stdin
#setopt stdin
### this is an interactive shell.
#setopt interactive


### Load a bunch of useful modules ###########################################

for mod in parameter zle zleparameter \
           compctl complete complist computil \
           deltochar mathfunc curses pcre bar zutil ; do
    zrcmodload zsh/${mod}
done
unset mod

zrcmodload -ab zsh/stat stat


### Mark a number of upstream functions for auto-loading #####################

for func in zargs zmv run-help{,-git,-svk,-svn} allopt ; do
    zrcautoload ${func}
done


### Mark all pricate functions for auto-loading ##############################

noauto=(vcs_info accept-line prompt_ft_setup lookupinit vcsupinit)
for func in ~/.zfunctions/**/[^_]*~*(~|.zwc)(N-.) ; do
    [[ -n ${(M)noauto:#${func:t}} ]] && continue
    zrcautoload ${func:t}
done
unset func noauto

# ...and initialise some of them:
zrcautoload lookupinit && lookupinit
zrcautoload init-atag && init-atag -q
zrcautoload accept-line && accept-line
zrcautoload edit-command-line && zle -N edit-command-line


### Figuring out keyboard sequences in the current terminal ##################

# The journey towards "the perfect key definitions"[tm].
#
#   THIS IS INCOMPLETE.
#
# We'll go about it like this:
#   - if there's zsh/terminfo and $terminfo[] "looks good", use it.
#   - if there's zsh/termcap and $termcap[] "looks good", use it.
#   - if neither is there, fall back to zkbd.
#   - if zkbd fails for some reason, create a setup-file-skeleton
#     for the terminal-OS combination in question.
#
# Slight deviation from the rules, we just established:
#   If the user marks a database entry as broken, directly fall back
#   to zkbd:
# % zstyle ':keyboard:$VENDOR:$OSTYPE:$TERM:*:*' broken (terminfo|termcap|both)
#
#   Also, allow for overwriting key definitions:
# % zstyle ':keyboard:$VENDOR:$OSTYPE:$TERM:terminfo:Home' overwrite $'\e[1~'
#
# Styles *have* to be set *before* sourcing this file.
# Also, this files expects pretty much zsh-mode default options. So,
# set your crazy options *after* sourcing this file.
#
# Note, that this file does *NOT* bind anything for you. It merely
# populates the $key[] hash, which you can later use to bind your keys,
# like this:
#   [[ -n ${key[Home]} ]] && bindkey ${key[Home]} beginning-of-line
#
# Also, this file uses a function called zprintf(), which I use in the
# rest of my setup. zsh doesn't know about it by default, so you'd need
# uncomment the following in order to avoid errors:
if [[ ${builtins[zmodload]} != 'defined' ]]; then
    zprintf 0 'keyboard: zmodload builtin not found, cannot go on.\n'
else
    [[ ${modules[zsh/parameter]} != 'loaded' ]] && zmodload zsh/parameter

    typeset -A kbd_terminfo_map
    typeset -A kbd_termcap_map

    kbd_terminfo_map=(
        Home        khome
        End         kend
        Insert      kich1
        Backspace   kbs
        Delete      kdch1
        Up          kcuu1
        Down        kcud1
        Left        kcub1
        Right       kcuf1
        PageUp      kpp
        PageDown    knp
    )

    kbd_termcap_map=(
        Home        kh
        End         @7
        Insert      kI
        Backspace   bs
        Delete      dc
        Up          ku
        Down        kd
        Left        kl
        Right       kr
        PageUp      kP
        PageDown    kN
    )

    mode='(-nothing-)'
    if [[ ${modules[zsh/terminfo]} == 'loaded' ]] &&
           [[ ${(t)terminfo} == association-*-special ]];
    then
        mode='terminfo'
    elif zmodload zsh/terminfo 2> /dev/null &&
            [[ ${modules[zsh/terminfo]} == 'loaded' ]] &&
            [[ ${(t)terminfo} == association-*-special ]];
    then
        mode='terminfo'
    elif [[ ${modules[zsh/termcap]} == 'loaded' ]] &&
             [[ ${(t)termcap} == association-*-special ]];
    then
        mode='termcap'
    elif zmodload zsh/termcap 2> /dev/null &&
            [[ ${modules[zsh/termcap]} == 'loaded' ]] &&
            [[ ${(t)termcap} == association-*-special ]];
    then
        mode='termcap'
    else
        mode='zkbd'
    fi

    zstyle -s ':keyboard:*' zkbddir zkbddir || zkbddir="${ZDOTDIR:-$HOME}/.zkbd"
    kcontext=":keyboard:${VENDOR}:${OSTYPE}:${TERM}:*:*"
    zstyle -s ${kcontext} broken broken || broken=''
    if [[ ${broken} == 'both' ]] || [[ ${broken} == ${mode} ]]; then
        mode='zkbd'
    fi

    if [[ ${mode} == 'zkbd' ]]; then
        function Printf_file() {
            [[ -f "$2" ]] && printf "$1" "$2" && return 0
            return 1
        }

        function zrc_printf_termfile() {
            Printf_file '%s' ~/.${zkbddir}/${TERM}-${VENDOR}-${OSTYPE} &&
                return 0
            Printf_file '%s' ~/.${zkbddir}/${TERM}-${DISPLAY} &&
                return 0
            return 1
        }

        termfile="$(zrc_printf_termfile)"
        if [[ -z "${termfile}" ]] ; then
            zrcautoload zkbd && zkbd
            termfile=$(zrc_printf_termfile)
        fi

        if [[ -f "${termfile}" ]] ; then
            zprintf 1 '  zle: loading %s\n' "${termfile}"
            source "${termfile}"
        else
            zprintf 0 'termfile (%s) not found. zkbd failed.\n' "${termfile}"
            mode='need-manual-skeleton'
        fi
        unfunction Printf_file zrc_printf_termfile

    elif [[ ${mode} == 'terminfo' ]]; then
        typeset -A key
        for k in ${(k)kbd_terminfo_map}; do
            key[$k]=${terminfo[${kbd_terminfo_map[$k]}]}
        done
        if [[ -n ${terminfo[smkx]} ]] && [[ -n ${terminfo[rmkx]} ]]; then
            function ft-ti-init () {
                echoti smkx
            }
            function ft-ti-deinit () {
                echoti rmkx
            }
            zle_init_functions=( ${zle_init_functions} ft-ti-init )
            zle_finish_functions=( ${zle_finish_functions} ft-ti-deinit )
        fi

    else # termcap
        typeset -A key
        for k in ${(k)kbd_termcap_map}; do
            key[$k]=${termcap[${kbd_termcap_map[$k]}]}
        done
        if [[ -n ${termcap[ks]} ]] && [[ -n ${termcap[ke]} ]]; then
            function ft-tc-init () {
                echotc ks
            }
            function ft-tc-deinit () {
                echotc ke
            }
            zle_init_functions=( ${zle_init_functions} ft-tc-init )
            zle_finish_functions=( ${zle_finish_functions} ft-tc-deinit )
        fi
    fi

    if [[ ${mode} == 'need-manual-skeleton' ]]; then
        termfile="${zkbddir}/MANUAL_${TERM}-${VENDOR}-${OSTYPE}"
        if [[ ! -e ${termfile} ]]; then
            printf '%s' "echo \"zkbd failed for terminal:" > ${termfile}
            printf '%s\n' " ${TERM}-${VENDOR}-${OSTYPE}\"" >> ${termfile}
            printf '%s\n' "echo \"This is ${termfile}\";echo" >> ${termfile}
            printf '%s' "echo \"Feel free to edit this" >> ${termfile}
            printf '%s\n' " file and manually insert the right\"" >> ${termfile}
            printf '%s' "echo \"sequences for" >> ${termfile}
            printf '%s\n\n' " the keys you want.\"" >> ${termfile}
            for k in ${(k)kbd_terminfo_map}; do
                printf '#key[%s]=$'\'''\''\n' $k >> ${termfile}
            done
        fi
        source ${termfile}
    fi

    key[Tab]='^I'
    for k in ${(k)key} ; do
        key[Alt-$k]='^['${key[$k]}
    done
    unset k

    function kbd_expand() {
        emulate -L zsh
        local k i
        setopt braceccl

        for k in {0..9} {a-z} {A-Z} . , \# + ; do
            for i in Ctrl- Alt- ; do
                case $i in
                (Ctrl-)
                    [[ $k != [a-z] ]] && continue
                    key[$i$k]='^'$k
                    ;;
                (Alt-)
                    # This might not work everywhere. Oh well...
                    key[$i$k]='^['$k
                    ;;
                esac
            done
        done
    }
    kbd_expand
    key[Alt-Dash]='^[-'
    key[Alt-Enter]='^[^M'

    unset mode kcontext broken kbd_terminfo_map \
          kbd_termcap_map termfile k sequence
    unfunction kbd_expand
fi

if [[ ${TERM} != 'dumb' ]] && zrcautoload colors && colors; then
    ZHAVE_COLORS=1
    # We got colour variables set, use them for less(1) setup:
    export LESS_TERMCAP_mb="${fg[cyan]}"
    export LESS_TERMCAP_md="${fg_bold[blue]}"
    export LESS_TERMCAP_me="${reset_color}"
    export LESS_TERMCAP_se="${reset_color}"
    export LESS_TERMCAP_so="${fg[yellow]}${bg[blue]}"
    export LESS_TERMCAP_ue="${reset_color}"
    export LESS_TERMCAP_us="${fg[magenta]}"
else
    ZHAVE_COLORS=0
fi

HELPDIR="~/.zhelp"
if check_com_print -c dircolors; then
    if [[ ${TERM} == screen* ]] ; then
        eval $( TERM=screen dircolors -b ~/.dircolors )
    else
        eval $( dircolors -b ~/.dircolors )
    fi
fi

# zstyle :completion:<function>:<completer>:<command>:<argument>:<tag> settings
function ft_compsys_setup() {
    zrcautoload compinit || return 1

    ### what completers to use?
    zstyle ':completion:*' completer _expand _complete _ignored _approximate

    ### use caching for all completions
    zstyle ':completion:*' use-cache on
    zstyle ':completion:*' cache-path ${HOME}/.zcache/

    ### add '..' dirs to completions
    zstyle ':completion:*' special-dirs ..

    ### use perl
    zstyle ':completion:*' use-perl on

    ### _urls setup
    zstyle ':completion:*' urls ~/etc/zsh/urls

    ### normal completion is nice; sometimes, when I'm misspelling
    ### something, the zsh should assists in finding the correct words
    ### '-e' is needed to get the argument evaluated each time this is called
    ### this argument to max-errors allows one error in 3 characters.
    ### found in 'From Bash to Z Shell' Chapter 10 - great book, btw :)
    ### '-e' was added in 4.0
    zstyle -e ':completion:*:approximate:*' \
           max-errors 'reply=( $(( ($#PREFIX + $#SUFFIX) / 3 )) )'

    ### formats & looks

    ### these set up the presentation of completion lists
    ### descriptions are printed in yellow fg color, sorted in groups
    if (( ${ZHAVE_COLORS} > 0 )) ; then
        zstyle ':completion:*:descriptions' format \
               "- %{${fg[yellow]}%}%d%{${reset_color}%} -"
        zstyle ':completion:*:messages'     format \
               "- %{${fg[cyan]}%}%d%{${reset_color}%} -"
        zstyle ':completion:*:corrections'  format \
               "- %{${fg[yellow]}%}%d%{${reset_color}%} - (%{${fg[cyan]}%}errors %e%{${reset_color}%})"
        zstyle ':completion:*:default'      select-prompt \
               "%{${fg[yellow]}%}Match %{${fg_bold[cyan]}%}%m%{${fg_no_bold[yellow]}%}  Line %{${fg_bold[cyan]}%}%l%{${fg_no_bold[red]}%}  %p%{${reset_color}%}"
        zstyle ':completion:*:default'      list-prompt   \
               "%{${fg[yellow]}%}Line %{${fg_bold[cyan]}%}%l%{${fg_no_bold[yellow]}%}  Continue?%{${reset_color}%}"
        zstyle ':completion:*:warnings'     format        \
               "- %{${fg_no_bold[red]}%}no match%{${reset_color}%} - %{${fg_no_bold[yellow]}%}%d%{${reset_color}%}"
        zstyle ':completion:*' group-name ''
    else
        zstyle ':completion:*:descriptions' format "- %d -"
        zstyle ':completion:*:messages'     format "- %d -"
        zstyle ':completion:*:corrections'  format "- %d - (errors %e)"
        zstyle ':completion:*:default'      select-prompt "Match %m  Line %l  %p"
        zstyle ':completion:*:default'      list-prompt "Line %l  Continue?"
        zstyle ':completion:*:warnings'     format "- no match - %d"
        zstyle ':completion:*'              group-name ''
    fi
    ### manual pages are sorted into sections
    zstyle ':completion:*:manuals'       separate-sections true
    zstyle ':completion:*:manuals.(^1*)' insert-sections   true

    ### reducing matches

    ### functions starting with '_' are completion functions by convention
    ### these are not supposed to be called by hand. no completion needed.
    zstyle ':completion:*:(functions|parameters|association-keys)' \
           ignored-patterns '_*'

    ### normally I don't want to complete *.o and *~, so, I'll ignore them
    ### update: these patterns are not ignored for rm, since i might want to
    ### delete those files
    #zstyle ':completion:*:(^rm):*:(all-|)files' ignored-patterns '*?.o' '*?\~'

    ### in my $HOME/bin dir there are often vim backups *~,
    ### these are not to be completed...
    ### i've got ${HOME}/bin/html/ which is no command, _do_not_complete_it!
    zstyle ':completion:*:complete:-command-::commands' \
           ignored-patterns '(aptitude-*|html|*\~|haskell_count)'

    ### don't complete lost+found dirs for 'cd'
    zstyle ':completion:*:cd:*' ignored-patterns '(*/)#lost+found'

    ### special completions for (mpg|ogg)123
    zstyle ':completion:*:*:mpg123:*' file-patterns \
           '(#i)*.mp3:files:mp3\ files *(-/):directories:directories'
    zstyle ':completion:*:*:ogg123:*' file-patterns \
           '(#i)*.ogg:files:ogg\ files *(-/):directories:directories'

    ### weed out uninteresting users when completing '(command) ~<TAB>'
    zstyle ':completion:*:*:*:*:users' ignored-patterns \
           Debian-exim  bitlbee games irc mail news postgres \
           uucp backup daemon gnats list man nobody proxy sync \
           www-data bin lp messagebus operator sys pokernetwork

    ### menu completion/selection setup

    ### enable menu completion
    zstyle ':completion:*' menu select

    ### enable verbose completion; descriptions like: '-a -- list entries
    ### starting with .'
    zstyle ':completion:*' verbose yes
    zstyle ':completion:*:-command-:*:' verbose no

    ### default menu selection for a few commands
    zstyle ':completion:*:*:(kill*|man):*' menu yes

    ### matching setup

    ### if i have 'rm file0' on the commandline
    ### i don't need "file0" in possible completions
    zstyle ':completion:*:(rm|kill):*' ignore-line yes

    ### $hosts for <tab> completing hostnames
    zstyle ':completion:*:(nc|ping|ssh|nmap|*ftp|telnet|finger):*' hosts $hosts

    ### i like kill <tab>, but i want more processes...
    zstyle ':completion:*:processes' command 'ps --forest -A -o pid,user,cmd'
    zstyle ':completion:*:*:kill:*:processes' sort false
    zstyle ':completion:*:processes-names' command \
           'ps c -u ${USER} -o command | uniq'

    ### list colorization

    ### use $LS_COLORS for general completion
    ###   Note: (s.:.) splits ${LS_COLORS} into an array
    zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}

    if (( ${ZHAVE_COLORS} > 0 )) ; then
        ### highlight parameters with uncommon names
        zstyle ':completion:*:parameters' \
               list-colors "=[^a-zA-Z]*=$color[red]"

        ### highlight aliases
        zstyle ':completion:*:aliases' \
               list-colors "=*=$color[green]"

        ### show that _* functions are not for normal use
        ### (not needed, since I don't complete _* functions at all)
        #zstyle ':completion:*:functions' \
        #    list-colors "=_*=$color[red]"

        ### highlight the original input.
        zstyle ':completion:*:original' \
               list-colors "=*=$color[red];$color[bold]"

        ### highlight words like 'esac' or 'end'
        zstyle ':completion:*:reserved-words' \
               list-colors "=*=$color[red]"

        ### colorize hostname completion
        zstyle ':completion:*:*:*:*:hosts' \
               list-colors "=*=$color[cyan];$color[bg-black]"

        ### colorize username completion
        zstyle ':completion:*:*:*:*:users' \
               list-colors "=*=$color[red];$color[bg-black]"

        ### colorize processlist for 'kill'
        zstyle ':completion:*:*:kill:*:processes' list-colors \
               "=(#b) #([0-9]#) #([^ ]#)*=$color[cyan]=$color[yellow]=$color[green]"
    fi

    ### initialize completion system
    compinit || return 1

    ### compdefs
    # must be done *after* compinit was called

    ### ping should complete hosts aswell...
    compdef _hosts ping

    ### my kmake() wrapper function should complete just as make does and add
    ### 'menuconfig' as a fake completion, because _make does not find it
    ### itself
    compdef kmake=make
    zstyle ':completion::complete:*kmake:*' fake \
           menuconfig -buildclean -catconfig

    [[ -n ${_comps[_quilt]} ]] && compdef dquilt=quilt

    # command specific setup
    zstyle ':completion:*:*:tmux:*:subcommands' ignored-patterns \
           'choose-*' 'confirm-before' 'find-window'
    zstyle ':completion:*:*:tmux:*:subcommands' mode 'commands'
    return 0
}
if ! ft_compsys_setup; then
    # +++ Fall back to zsh's old completion system (compctl) +++
    zrcmodload zsh/compctl || zrcmodload compctl

    ### complete only dirs (or symlinks to dirs in some cases) for certain
    ### commands
    compctl -g '*(/)' rmdir dircmp
    compctl -g '*(-/)' cd chdir dirs pushd

    ### hosts completion for a few commands
    compctl -k hosts ftp lftp ncftp ssh w3m lynx links elinks nc telnet rlogin host
    compctl -k hosts -P '@' finger

    ### kill
    compctl -j -P '%' + -s '`ps ax | tail -n+2 | cut -c1-5`' + -x 's[-] p[1]' \
            -k "($signals[1,-3])" -- kill

    ### su
    compctl -u -x 'w[2,-c] p[3,-1]' -l '' -- su

    ### id passwd
    compctl -u id passwd

    ### mount
    function mounttab() {
        reply=(`egrep "^/.*" /etc/fstab | awk '{print $1}'`)
    }
    compctl -K mounttab mount

    ### umount
    function umounttab() {
        reply=(`mount | cut -d' ' -f3`)
    }
    compctl -K umounttab umount

    # filetypes based completion
    compctl -g '*.Z *.gz *.tgz' + -g '*' zcat zless zgrep gunzip gzip
    compctl -g '*.bz2' + -g '*' bzip2 bunzip2
    compctl -g '*.tar.Z *.tar.gz *.tgz *.tar.bz2' + -g '*' tar
    compctl -g '*.zip *.ZIP' + -g '*' unzip zip
    compctl -g '*(-/) *.pl *.PL *.cgi *.pm *.PM *.t *.xpl' perl
    compctl -g '*(-/) *.pl *.PL *.pm *.PM *.pod' -K perldoc pod
    compctl -g '*(-/) *.rb' ruby
    compctl -g '*(-/) *.py *.pyc' python
    compctl -g '*(-/) *.c' splint

    function listscreens() {
        reply=(`screen -ls|grep 'tached'|sed -e 's/  //'|sed -e 's/  .*//'`)
    }
    compctl -K listscreens screen

    function makeentry() {
        local a
        local mfile
        read -cA a
        mfile=(GNUmakefile makefile Makefile)
        while [ ! -z $a[0] ]; do
            shift a
            case $a[0] in
            -f)
                shift a;
                mfile=$(a[0])
                ;;
            esac
        done
        while [ ! -z "$mfile[0]" ]; do
            if [ -f $mfile[0] ]; then
                reply=(`egrep '^[^#. ][^=       ]*:' $mfile[0] | cut -d: -f1`)
                break
            else
                shift mfile
            fi
        done
    }
    compctl -K makeentry -x 'c[-1,-f]' -f -- make

    function remote_files() {
        local a
        read -cA a
        reply=(`ssh ${a[-1]%%:*} "echo ${a[-1]#*:]*/(/N) ${a[-1]#*:}*(.N)"`)
    }
    function sshhosts() {
        reply=(`cut -d' ' -f1 ~/.ssh/known_hosts | cut -d, -f1`)
    }
    compctl -K sshhosts -k hosts -x 'c[-1,-1]' -u -- ssh slogin sftp
    compctl                                                                 \
        -k sshhosts -S ':'                                                  \
        -g '*(-/) *' -S ' '                                                 \
        -u -S '@'                                                           \
        -x 'n[-1,@]' -K sshhosts -S ':'                                     \
        - 's[-]' -k '(a A q Q p r v B C L S o P c i)'                       \
        - 'c[-1,-S]' -X '' -f                                               \
        - 'c[-1,-l]' -u                                                     \
        - 'c[-1,-o]' -X ''                                                  \
        - 'c[-1,-P]' -X ''                                                  \
        - 'c[-1,-c]' -X '' -k '(idea blowfish des 3des arcfour tss none)'   \
        - 'c[-1,-i]' -X '' -f                                               \
        - 'n[-1,:]' -S '' -K remote_files                                   \
        - 'C[0,[./]*] ' -f                                                  \
        -g '*(-/) *' -S ' ' -- scp

    function man_glob() {
        local a mp MP
        MP=${MANPATH:-/usr/share/man:/usr/X11R6/man:/usr/local/man:/usr/local/share/man}
        mp=(${(s+:+)MP})
        read -cA a
        case $a[2] in
        1|2|3|4|5|6|7|8|9)
            reply=(${^mp}/man$a[2]/$1*$2(N:t:fr))
            ;;
        *)
            reply=(${^mp}/man*/$1*$2(N:t:fr))
            ;;
        esac
    }
    compctl -K man_glob man whatis apropos

    ### completion for some builtins (taken from examples)
    compctl -z -P '%' bg
    compctl -j -P '%' fg jobs disown
    compctl -j -P '%' + -s '`ps -x | tail +2 | cut -c1-5`' wait
    compctl -A shift
    compctl -cAF type whence where which
    compctl -m -x 'W[1,-*d*]' -n - 'W[1,-*a*]' -a - 'W[1,-*f*]' -F -- unhash
    compctl -m -q -S '=' -x 'W[1,-*d*] n[1,=]' -g '*(-/)' - 'W[1,-*d*]' -n -q \
            -S '=' - 'n[1,=]' -g '*(*)' -- hash
    compctl -F functions unfunction
    compctl -k '(al dc dl do le up al bl cd ce cl cr dc dl do ho is le ma nd nl se so up)' echotc
    compctl -a {,un}alias
    compctl -v getln getopts read unset vared
    compctl -v -S '=' -q declare export integer local readonly typeset
    compctl -eB -x 'p[1] s[-]' -k '(a f m r)' - 'C[1,-*a*]' -ea - 'C[1,-*f*]' \
            -eF - 'C[-1,-*r*]' -ew -- disable
    compctl -dB -x 'p[1] s[-]' -k '(a f m r)' - 'C[1,-*a*]' -da - 'C[1,-*f*]' \
            -dF - 'C[-1,-*r*]' -dw -- enable
    compctl -k "(${(j: :)${(f)$(limit)}%% *})" limit unlimit
    compctl -l '' -x 'p[1]' -f -- . source
    compctl -s '$(setopt 2>/dev/null)' + -o + -x 's[no]' -o -- unsetopt
    compctl -s '$(unsetopt)' + -o + -x 's[no]' -o -- setopt
    compctl -s '${^fpath}/*(N:t)' autoload
    compctl -s '${^module_path}/*(N:t:r)' \
            -x 'W[1,-*(a*u|u*a)*],W[1,-*a*]p[3,-1]' -B - 'W[1,-*u*]' \
            -s '$(zmodload)' -- zmodload
    compctl -b bindkey
    compctl -c -x 'C[-1,-*k]' -A - 'C[-1,-*K]' -F -- compctl
    compctl -x 'C[-1,-*e]' -c - 'C[-1,-[ARWI]##]' -f -- fc
    compctl -x 'p[1]' - 'p[2,-1]' -l '' -- sched
    compctl -x 'C[-1,[+-]o]' -o - 'c[-1,-A]' -A -- set
    compctl -l '' nohup noglob exec nice eval time sudo
fi

# chpwd_profiles(): Directory Profiles
#
# Say you want certain settings to be active in certain directories. This is
# what you want.
#
# To get it working you will need this function and something along the
# following lines:
#
#   chpwd_functions+=( chpwd_profiles )
#   chpwd_profiles
#
# You will usually want to do that *after* you configured the system. That
# configuration is described below.
#
# zstyle ':chpwd:profiles:/usr/src/grml(|/|/*)'   profile grml
# zstyle ':chpwd:profiles:/usr/src/debian(|/|/*)' profile debian
#
# When that's done and you enter a directory that matches the pattern in the
# third part of the context, a function called chpwd_profile_grml, for example,
# is called (if it exists).
#
# If no pattern patches (read: no profile is detected) the profile is set to
# 'default', which means chpwd_profile_default is attempted to be called.
#
# A word about the context (the ':chpwd:profiles:*' stuff in the zstyle
# command) which is used: The third part in the context is matched against
# ${PWD}. That's why using a pattern such as /foo/bar(|/|/*) makes sense.
# Because that way the profile is detected for all these values of ${PWD}:
#   /foo/bar
#   /foo/bar/
#   /foo/bar/baz
# So, if you want to make double damn sure a profile works in /foo/bar and
# everywhere deeper in that tree, just use (|/|/*) and be happy.
#
# The name of the detected profile will be available in a variable called
# 'profile' in your functions. You don't need to do anything, it'll just be
# there.
#
# Then there is the parameter $CHPWD_PROFILE is set to the profile, that was is
# currently active (the default value is "default"). That way you can avoid
# running code for a profile that is already active, by running code such as
# the following at the start of your function:
#
# function chpwd_profile_grml() {
#     [[ ${profile} == ${CHPWD_PROFILE} ]] && return 1
#   ...
# }
#
# If you know you are going to do that all the time for each and every
# directory-profile function you are ever going to write, you may also set the
# `re-execute' style to `false' (which only defaults to `true' for backwards
# compatibility), like this:
#
#   zstyle ':chpwd:profiles:*' re-execute false
#
# If you use this feature and need to know whether it is active in your current
# shell, there are several ways to do that. Here are two simple ways:
#
# a) If knowing if the profiles feature is active when zsh starts is good
#    enough for you, you can use the following snippet:
#
# (( ${+functions[chpwd_profiles]} )) && print "directory profiles active"
#
# b) If that is not good enough, and you would prefer to be notified whenever a
#    profile changes, you can solve that by making sure you start *every*
#    profile function you create like this:
#
# function chpwd_profile_myprofilename() {
#     [[ ${profile} == ${CHPWD_PROFILE} ]] && return 1
#     print "chpwd(): Switching to profile: $profile"
#   ...
# }
#
# That makes sure you only get notified if a profile is *changed*, not
# everytime you change directory. (To avoid this, you may also set the newer
# `re-execute' style like described further above instead of the test on top of
# the function.
#
# If you need to do initialisations the first time `chpwd_profiles' is called
# (which should be in your configuration file), you can do that in a function
# called "chpwd_profiles_init". That function needs to be defined *before*
# `chpwd_profiles' is called for this to work.
#
# During the *first* call of `chpwd_profiles' (and therefore all its profile
# functions) the parameter `$CHPWD_PROFILES_INIT' exists and is set to `1'. In
# all other cases, the parameter does not exist at all.
#
# When the system switches from one profile to another, it executes a function
# named "chpwd_leave_profile_<PREVIOUS-PROFILE-NAME>()" before calling the
# profile-function for the new profile.
#
# There you go. Now have fun with that.
#
# Note: This feature requires zsh 4.3.3 or newer.
function chpwd_profiles() {
    local profile context
    local -i reexecute

    context=":chpwd:profiles:$PWD"
    zstyle -s "$context" profile profile || profile='default'
    zstyle -T "$context" re-execute && reexecute=1 || reexecute=0

    if (( ${+parameters[CHPWD_PROFILE]} == 0 )); then
        typeset -g CHPWD_PROFILE
        local CHPWD_PROFILES_INIT=1
        (( ${+functions[chpwd_profiles_init]} )) && chpwd_profiles_init
    elif [[ $profile != $CHPWD_PROFILE ]]; then
        (( ${+functions[chpwd_leave_profile_$CHPWD_PROFILE]} )) \
            && chpwd_leave_profile_${CHPWD_PROFILE}
    fi
    if (( reexecute )) || [[ $profile != $CHPWD_PROFILE ]]; then
        (( ${+functions[chpwd_profile_$profile]} )) && chpwd_profile_${profile}
    fi

    CHPWD_PROFILE="${profile}"
    return 0
}

zstyle ':chpwd:profiles:'${HOME}'/src/sys/zsh.git(|/|/*)' profile zshsrc
zstyle ':chpwd:profiles:'${HOME}'/src/sys/git(|/|/*)' profile gitsrc
zstyle ':chpwd:profiles:'${HOME}'/src/code/cmus(|/|/*)' profile cmussrc
zstyle ':chpwd:profiles:*' re-execute false

function chpwd_switch_profile_ () {
    [[ $profile == default ]] && [[ $CHPWD_PROFILES_INIT == 1 ]] && return 0
    print "chpwd(): Switching to profile: $profile"
}

function chpwd_profile_zshsrc() {
    chpwd_switch_profile_ || return 1
    export VIM_PLEASE_USE_INDENT='3'
}

function chpwd_profile_cmussrc() {
    chpwd_switch_profile_ || return 1
    export VIM_PLEASE_USE_INDENT='4'
}

function chpwd_profile_gitsrc() {
    chpwd_profile_cmussrc
}

function chpwd_profile_default() {
    chpwd_switch_profile_ || return 1
    export VIM_PLEASE_USE_INDENT='8'
}

# get the right profile running
chpwd_functions+=( chpwd_profiles )
chpwd_profiles


### Prompt Setup #############################################################

function zrcprompt() {
    setopt localoptions
    local load_prompt=$1

    zrcautoload promptinit && promptinit || return 1
    zrcautoload prompt_ft_setup || return 1
    prompt ${load_prompt}
    zrcautoload add-zsh-hook && add-zsh-hook precmd precmd_status
    return 0
}
if zrcprompt ft ; then
    ZNEED_OLD_PROMPT=0
    # My prompt will use psvar1 as the current keyboard-mode, if this style is
    # set to `true'. The default is `false'.
    zstyle ':prompt:ft:*' use-psvar1 true
else
    ZNEED_OLD_PROMPT=1
    zprintf 0 'FAILED to load prompt theme system!\n'
fi
unfunction zrcprompt

# old prompt
#   my old prompt, not using promptinit, but works everywhere.
#   no vcs_info support.

# simple prompt; i _love_ simplicity; (okay, it got a little colorful by now)
#   well, it looks pretty messy at first sight.
#
#   so, here we go:
#
#     %(?..[%?]-)         will display      '[return-code]-'
#                         but only if the last command returned non-zero
#     %1(j.(j:%j%)-.)     displays          '(j: job-count)-'
#                         but only if you got 1 or more background jobs running
#     (%!)-               always displays   '(history number)-'
#     %3~                 displays the last 3 dirs you're in. eg.
#                         'src/project-x.y.z/src' when you're in
#                         '/usr/src/project-x.y.z/src'
#     %#                  '#' for root; '%' for regular users
#
#   so the prompt may look like this:
#     (12345)-/usr/src%
#     [130]-(12345)-/usr/src%
#     (j: 2)-(12345)-/usr/src%
#     [130]-(j: 2)-(12345)-/usr/src%
#
# If I'm in a git repo, my prompt tells me about the currently checked out
# branch:
#   (12345)-/usr/src [master]-%

if (( ZNEED_OLD_PROMPT > 0 )); then
    zprintf 2 'Old prompt is not needed. Skipping.\n'
else
    zprintf 1 'Falling back to old prompt definition!\n'

    if (( ZHAVE_COLORS > 0 )) ; then
        # color hashes for prompts:
        #   because this:
        #       $PBfg[ma]
        #   is a lot shorter (and easier to read) then this:
        #       %{$fg_bold[magenta]%}
        typeset -A amap cmap
        amap=(
            'fg'        Pfg
            'fg_bold'   PBfg
            'bg'        Pbg
            'bg_bold'   PBbg
        )
        cmap=(
            cyan    cy
            white   wh
            yellow  ye
            magenta ma
            black   bla
            blue    blu
            red     red
            grey    gry
            green   grn
            default def
        )

        typeset -g PNC='%{'${reset_color}'%}'
        for ar in ${(k)amap} ; do
            typeset -gA ${amap[$ar]}
            for col in ${(Pk)ar} ; do
                [[ -z ${cmap[$col]} ]] && continue
                typeset -g ${amap[$ar]}'['${cmap[$col]}']'='%{'${(P)${:-${ar}'['${col}']'}}'%}'
            done
        done
        unset amap cmap
    fi

    # These are the pieces that are used to setup $PS1
    P_RETVAL="%(?..${Pfg[wh]}${Pbg[red]}[%?]${PNC}-)"
    P_JOBNUM="%1(j.(${Pfg[cy]}j: %j${PNC}%)-.)"
    P_HISTEV="(${Pfg[red]}%!${PNC}%)"
    P_SHPATH="${Pfg[ye]}%3~${PNC}"
    # my prompt, heavily colored
    Z_PROMPT="${P_RETVAL}${P_JOBNUM}${P_HISTEV}-${P_SHPATH}%# "
    Z_OLD_PROMPT="${P_RETVAL}${P_HISTEV}-${P_SHPATH}%# "
    # super simple
    Z_SIMPLE_PROMPT="zsh%# "

    PROMPT=${Z_PROMPT}"${Pfg[cy]}"
    : ${(qqq)PROMPT}
    (( ZHAVE_COLORS > 0 )) && POSTEDIT="${reset_color}"

    # spelling-correction prompt
    if (( ZHAVE_COLORS > 0 )); then
        P_WRONG=\'"${Pfg[ye]}%R${PNC}"\'
        P_CORRECTED=\'"${Pfg[ma]}%r${PNC}"\'
        P_CHOICES="${Pfg[ye]}Y${Pfg[ma]}N${Pfg[grn]}E${Pfg[red]}A${PNC}"
        SPROMPT="zsh: correct ${P_WRONG} to ${P_CORRECTED}? (${P_CHOICES}) "
        unset P_WRONG P_CORRECTED P_CHOICES
    fi

    # right-prompt; tell me where I am, if I'm on a remote machine
    if [[ -n ${SSH_CLIENT} ]] ; then
        RPROMPT="${Pfg[red]}"
        RPROMPT="$RPROMPT"'['"${Pfg[cy]}%n${PNC}@${Pfg[cy]}%m${Pfg[red]}"
        RPROMPT="$RPROMPT"']'"${Pfg[cy]}"
    fi

    unset ZNEED_OLD_PROMPT
fi


### Pre-Cmd and -Exec Hooks (terminal title setting) #########################

if [[ -n ${functions[add-zsh-hook]} ]]; then
    function screen_printf () {
        # This should be pretty safe for most strings that are thrown at us.
        local format esc
        esc=$'\e'
        format="$1"
        shift
        printf '%s' "${esc}k"
        printf "${format}" "$@"
        printf '%s' "${esc}"\\
    }

    # put the current program (or 'zsh') into screen's hardstatus
    function precmd_status () {
        local zsh
        if [[ "$TERM" == screen* ]]; then
            if [[ -n ${vcs_info_msg_1_} ]] ; then
                zsh=${vcs_info_msg_1_}
            else
                zsh='zsh'
            fi
            if [[ -n ${SSH_CLIENT} ]] ; then
                screen_printf '%s: %s' "${(%):-"%m"}" "${zsh}"
            else
                screen_printf '%s' "${zsh}"
            fi
        fi
    }

    function preexec_status () {
        setopt localoptions extendedglob
        local CMD
        local -a words

        if [[ "$TERM" != screen* ]]; then
            return 1
        fi
        words=( ${(z)1} )
        while (( ${#words} > 0 )) ; do
            case ${words[1]} in
                *=*)    shift words ;;
                sudo)   shift words ;;
                -*)     shift words ;;
                *) break ;;
            esac
        done

        case ${words[1]} in
            git|cvs|svn|bzr|hg|p4)
                CMD="${words[1]} ${words[2]}" ;;
            *)  CMD="${words[1]}" ;;
        esac

        if [[ -n ${SSH_CLIENT} ]] ; then
            screen_printf '%s: %s' "${(%):-"%m"}" "${CMD}"
        else
            screen_printf '%s' "${CMD}"
        fi
    }

    add-zsh-hook precmd precmd_status
    add-zsh-hook preexec preexec_status
fi


### Configurable Function Setups #############################################

# Some of the functions in my "~/.zfunctions" folder are configurable. That
# configuration is done here.

# +++ Accept-Line +++

zstyle ':acceptline:empty' call_default false
zstyle ':acceptline:*' nocompwarn true

function turn_dots_into_cd () {
    local buf="$1"
    local -i i
    buf='cd '
    for (( i = 1; i <= ${#${cmdline[1]}}; i++ )); do
        buf="${buf}../"
    done
    buf=${buf%/}
    REPLY="$buf"
}

function zle_cd_back() {
    local REPLY
    if (( ${#cmdline} == 1 )) && [[ ${cmdline[1]} == .# ]] ; then
        turn_dots_into_cd "${BUFFER}"
        BUFFER="$REPLY"
    fi
}

function zle_dir_stack() {
    if (( ${#cmdline} == 1 )); then
        case ${cmdline[1]} in
        ((+|-)(|<->))
            BUFFER="cd $BUFFER"
            [[ ${cmdline[1]} == + ]] && BUFFER="${BUFFER}0"
            ;;
        ((#b)-([lcvp]))
            BUFFER="dirs -${match[1]}"
            ;;
        esac
    fi
}

zle -N zle_cd_back
zle -N zle_dir_stack
zstyle ':acceptline:preprocess' actions zle_cd_back zle_dir_stack


# +++ gitfp +++

if (( ${+functions[gitfp]} )); then
    function +gitfp_zsh_ps_fix() {
        # zsh-workers prefers PATCH: as its prefix.
        #   fixes [PATCH] to PATCH:
        #     and [PATCH m/n] to PATCH: (m/n)

        [ -z "$1" ] && return 1

        (
            printf '/^Subject: \[\n'
            printf 's,\[PATCH \([0-9]\+/[0-9]\+\)\],PATCH: (\\1),\n'
            printf 'w\nq\n'
        ) | ed "$1" > /dev/null 2>&1

        [ "$?" -eq 0 ] && return 0

        (
            printf '/^Subject: \[\n'
            printf 's,\[PATCH\],PATCH:,\n'
            printf 'w\nq\n'
        ) | ed "$1" > /dev/null 2>&1

        return 0
    }
    # The 3rd part of the context is the currently active profile
    # from the directory based contexts, which are setup above.
    zstyle ':functions:gitfp:zshsrc' hooks +gitfp_zsh_ps_fix
    zstyle ':functions:gitfp:zshsrc' options --src-prefix= --dst-prefix=
else
    zprintf 2 'No `gitfp'\'' function in use, skipping setup.\n'
fi


if (( ${+functions[lookup]} )); then
    zstyle ':lookup:*'         use-pager   true
    zstyle ':lookup:*'         pager-auto  true
    zstyle ':lookup:*'         pager       'less -M'
    zstyle ':lookup:*'         txt-browser 'w3m'
    zstyle ':lookup:*'         txt-formats '%s'
    zstyle ':lookup:*:rfc:*'   search-path ~/share/rfc /usr/share/doc/RFC/links
    zstyle ':lookup:*:rfc:*'   save-path   ~/share/rfc

    zstyle ':lookup:*' gui-browser firefox
    zstyle ':lookup:*' gui-formats -p generic -new-tab '%s'

    # Here's a nice one: I got this script cmus-rstat.sh, which tells me which
    # track cmus (my audio player) is currently playing in this format:
    #       np: Artist - Album - Track
    #
    # Well, that's easily parsable. :)
    # Now I can make automated searches via letssingit.com with lookup's
    # query-handler feature like this:
    #
    # If I want to search for the artist:
    #   % lookup -Q letssingit -m artist
    # or the album:
    #   % lookup -Q letssingit -m album
    # or the track name (for instant sing-along :->):
    #   % lookup -Q letssingit -m song
    #
    zstyle ':lookup:*:letssingit:*' query-handlers letssingit
    function LOOKUP_qh_letssingit() {
        local mode=${lookup_communicate[mode]}
        local np

        case ${mode} in
        (artist|album|song)
            np="$(cmus-rstat.sh)"
            np=${np/np: /}
            if [[ ${np} == '[cmus]: -stopped-' ]] ; then
                printf '\n  Cmus is currently playing nothing!\n\n'
                return 3
            fi
            ;;
        esac

        case ${mode} in
        (artist)
            QUERY=${np%% - *}
            ;;
        (album)
            np=${np% - *}
            if [[ ${np} == *' - '* ]] ; then
                QUERY=${np#* - }
            else
                printf '\n Could not get album (%s)!\n\n' "$(cmus-rstat.sh)"
                return 3
            fi
            ;;
        (song)
            QUERY=${np##* - }
            ;;
        esac

        return 0
    }

    # aliases for lookup backends
    lookup -a s=google
    lookup -a d=leo
    lookup -a we="wikipedia -l en"
    lookup -a wd="wikipedia -l de"
else
    zprintf 2 'Lookup is not in use, skipping setup.\n'
fi

if (( ${+functions[hl]} )); then
    zstyle ':functions:hl:*:*-256color:dark:*' theme darkslategray
    zstyle ':functions:hl:*:*-256color:light:*' theme edit-msvs2008
else
    zprintf 2 'hl is not in use, skipping setup.\n'
fi

if (( ${+functions[vcs_info]} )); then
    # Load vcsup addon.
    zrcautoload vcsupinit && vcsupinit
    zstyle ':vcsup:*' stack-auto-push true
    zstyle ':vcsup:*' stack-no-dups true

    zstyle ':vcs_info:*' max-exports 2
    zstyle ':vcs_info:*' actionformats \
           " %F{magenta}(%f%s%F{magenta})%F{yellow}-%F{magenta}[%F{green}%b%F{yellow}|%F{red}%a%F{magenta}]%f-" \
           "zsh: %r"
    zstyle ':vcs_info:*' formats \
           " %F{magenta}(%f%s%F{magenta})%F{yellow}-%F{magenta}[%F{green}%b%F{red}+%Q%F{magenta}]%f%}-" \
           "zsh: %r"
    zstyle ':vcs_info:git:*' actionformats \
           " %F{magenta}[%F{green}%b%F{red}%c%u%F{yellow}|%F{red}%a%F{magenta}]%f-" \
           "zsh: %r"
    zstyle ':vcs_info:git:*' formats \
           " %F{magenta}[%F{green}%b%F{red}%c%u%F{magenta}]%f-" \
           "zsh: %r"
    zstyle ':vcs_info:git-svn:*' actionformats \
           " %F{magenta}[%F{yellow}gitsvn%f:%F{green}%b%F{red}%c%u%F{yellow}|%F{red}%a%F{magenta}]%f-" \
           "zsh: %r"
    zstyle ':vcs_info:git-svn:*' formats \
           " %F{magenta}[%F{yellow}gitsvn%f:%F{green}%b%F{red}%c%u%F{magenta}]%f-" \
           "zsh: %r"
    zstyle ':vcs_info:cvs:*' formats \
           " %F{magenta}(%f%s%F{magenta})%F{yellow}-%F{magenta}[%F{green}%r%F{magenta}]%f-" \
           "zsh: %r"
    zstyle ':vcs_info:(sv[nk]|bzr):*' branchformat "%b%F{red}:%F{yellow}%r"
    zstyle ':vcs_info:bzr:*' use-simple true
    zstyle ':vcs_info:*'     enable git hg cvs svn bzr

    function estyle-cfc() {
        local d
        local -a cfc_dirs
        cfc_dirs=( ${HOME}/src/code/*(/) )
        for d in ${cfc_dirs}; do
            d=${d%/##}
            [[ $PWD == $d(|/*) ]] && return 0
        done
        return 1
    }

    #zstyle -e ':vcs_info:git:*' check-for-changes \
    #       'estyle-cfc && reply=( true ) || reply=( false )'
    #zstyle ':vcs_info:git:*' check-for-changes true

    ### quilt setup

    zstyle ':vcs_info:*' use-quilt true
    zstyle ':vcs_info:*.quilt-addon:*' patch-format "%n"
    zstyle ':vcs_info:*.quilt-addon:*' nopatch-format "0"
    zstyle ':vcs_info:*' quilt-get-unapplied true
    zstyle ':vcs_info:*.quilt-standalone:*:*' quilt-patch-dir debian/patches
    zstyle ':vcs_info:-quilt-:*' \
           formats " %F{magenta}[%F{green}%Q%F{ma}]%f%}-" \
           "zsh: %r"

    ### hooks
    #zstyle ':vcs_info:*+*:*' debug true
else
    zprintf 2 'Vcs_info is not in use, skipping setup.\n'
fi

if (( ${+functions[ta]} )); then
    zstyle ':fnc:ta:*' pager less -M
    function TA/kill-message () {
        [[ $1 != 0 ]] && return 0
        local -a reply
        +ta+format-strings '%c' '%a'
        "${tmux_cmd[@]}" display-message -c ${reply[1]} -t $session \
                         "ta: Killed session: ${reply[2]}"
        return 0
    }
    function TA/really-kill () {
        local -a reply
        +ta+format-strings '%a'
        local p=${reply[1]}
        printf 'ta: Do you actually want to kill session `%s'\''?\n' $p
        printf 'ta: Type "yes" to confirm; anything else cancels.\n'
        local ans
        read ans
        [[ $ans == yes ]] && return 0
        unset 'opt[-k]'
        return 1
    }
    function TA/three-shells () {
        [[ $session == (quake|d/*) ]] || return 0
        local wd=$1
        repeat 2; do "${tmux_cmd[@]}" new-window -c $wd -t $session; done
        repeat 2; do "${tmux_cmd[@]}" previous-window -t $session; done
        return 0
    }
    zstyle ':fnc:ta:*' pre-kill-previous-session-hook TA/kill-message
    zstyle ':fnc:ta:*' post-session-creation-hook TA/three-shells
    zstyle ':fnc:ta:*k*:*:*:*:misc:*' pre-dispatch-hook TA/really-kill
    zstyle ':completion:*:*:ta:*:*' group-order sessions candidates
    if [[ -d ~/src/prj ]]; then
        # Looks like a private system. Set a candidate generator.
        function TA/candidates () {
            reply=( quake d/grml d/guix
                    ~/src/prj/*~*/[A-Z]*(/e@'REPLY=d/${REPLY:t}'@) )
            return 0
        }
        zstyle ':completion:*:*:ta:*:*' generate TA/candidates
        # We can calculate the working directory of most candidate
        # sessions, too:
        function TA/compute-working-directory () {
            local dir
            if [[ $session == d/grml ]]; then
                dir=$HOME/src/grml/grml-etc-core
            elif [[ $session == d/guix ]]; then
                dir=$HOME/src/guix
            else
                dir=$HOME/src/prj/${session#d/}
            fi
            if [[ -d $dir ]]; then
                reply=$dir
            else
                reply=$HOME
            fi
            return 0
        }
        zstyle -e ':fnc:ta:*' working-directory TA/compute-working-directory
    fi
else
    zprintf 2 'Tmux utility "ta" is not in available, skipping setup.\n'
fi


### Aliases ##################################################################

### normal user aliases
###   Note: normally aliases that redefine well-known commands are not
###         considered to be good practice, since other users might get
###         confused by changed environments. These ls- and grep-aliases should
###         be okay, since '--color=auto' only adds color sequences, when the
###         output goes to a terminal. ...smart little helpers, indeed. :-)
###
if [[ $(ls --version 2> /dev/null) == ((*GNU*)|(*fileutils*)|(*coreutils*)) ]];
then
    alias ls='ls --color=auto'
    alias lsc='ls --color=always'
elif [[ -x $(which gls) ]] &&
         [[ $(gls --version) == ((*GNU*)|(*fileutils*)|(*coreutils*)) ]];
then
    alias ls='gls --color=auto'
    alias lsc='gls --color=always'
else
    xalias ls='ls -F'
fi
if echo zsh | grep --color=auto zsh > /dev/null 2>&1 ; then
    alias grep='grep --color=auto'
    alias grepc='grep --color=always'
fi
[[ -f ${HOME}/etc/x/aliasX ]] && \
    alias X='exec startx >& ${HOME}/.Xlog'

### a fresh zsh, please.
alias fzsh='PS1="zsh%# " zsh -f'

### generic aliases (ie. no special checks needed, using xalias().
xalias gpw="makepasswd --chars 12 --string 'abcdefghijklmnopqrrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+*%_-=.,!|/~'"
xalias gpwns="makepasswd --chars 12 --string 'abcdefghijklmnopqrrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-'"
xalias rot13='tr A-Za-z N-ZA-Mn-za-m'

### Globbing with git doesn't make a lot of sense with me, similarly mmh.
alias git='noglob git'
alias mmh='noglob mmh'

### get status from cmus
xalias np='cmus-rstat.sh'

alias  stat='command stat'
alias zstat='builtin stat'

### the run-help alias is not defined in older versions
unalias run-help &> /dev/null

### svk does not honour $VISUAL
alias svk='EDITOR='${VISUAL}' svk'

# Octave without all the nonsense.
alias oc='octave --quiet --no-gui'

### cvs is a little noisy when it recurses.
xalias cvs='cvs -q'

### sudo aliases

### reboot/halt/suspend
salias -c -o reboot='/sbin/reboot'
salias -c -o shutdown='/sbin/shutdown'
salias -c -o halt='/sbin/halt'
salias -c -o hibernate='/usr/sbin/hibernate'

### process control
salias -c -o kill='kill'
salias -c -o killall='killall'

### debian tools
if [[ -f /etc/debian_version ]] ; then
    salias -c -o aptitude='aptitude'
    salias -c -o apt='apt'
    salias -c -o apt-get='apt-get'
    salias -c -o apt-cache='apt-cache'
    salias -c -o apt-file='apt-file'
    salias -c -o apt-key='apt-key'
    salias -c -o apt-secure='apt-secure'
    salias -c -o ifup='/sbin/ifup'
    salias -c -o ifdown='/sbin/ifdown'
    salias -c -o pon='pon'
    salias -c -o poff='poff'
    salias -c -o plog='plog'
fi

### admin
salias -c -o ifconfig='/sbin/ifconfig'
salias -c -o modprobe='/sbin/modprobe'
salias -c -o insmod='/sbin/insmod'
salias -c -o depmod='/sbin/depmod'

### network::misc
salias -c -o arping='/usr/sbin/arping'
salias -c -o nmap='nmap'
salias -c    sniff='tshark'
salias -c -o kismet='kismet'
salias -c -o kismet_server='kismet_server'
salias -c -o kismet_drone='kismet_drone'
salias -c -o kismet_client='kismet_client'

### abcde, for real-time features of cdda2wav
salias -c -o abcde='abcde'

### names for certain directories
###   you can do 'cd ~cmus' to cd to /usr/src/cmus, for example
###

xhashd audio=/mnt/audio
xhashd cmus=${HOME}/src/code/cmus
xhashd doc=/usr/share/doc
xhashd fvwm=${HOME}/etc/fvwm
xhashd git=/usr/src/git
xhashd howto=/usr/share/doc/HOWTO/en-txt
xhashd hs=/mnt/audio/hoerspiel
xhashd irssi=${HOME}/etc/irssi
xhashd music=/mnt/audio/music
xhashd mutt=${HOME}/etc/mutt
xhashd screen=${HOME}/etc/screen
xhashd slrn=${HOME}/etc/slrn
xhashd src=/usr/src
xhashd vim=${HOME}/etc/vim
xhashd web=/var/www
xhashd zdir=${${${(M)fpath:#*/zsh/${ZSH_VERSION}/*}[1]}%${ZSH_VERSION}*}${ZSH_VERSION}
xhashd zsh=${HOME}/.zshrc.d

# Use bracketed url paste magic
autoload -Uz bracketed-paste-url-magic
zle -N bracketed-paste bracketed-paste-url-magic

# ‘insert-by-context’ needs ‘read-from-minibuffer’
autoload -Uz read-from-minibuffer
autoload -Uz insert-by-context

# Define ‘insert-by-context’ as a zle widget
zle -N insert-by-context

# Turn ‘insert-tab’ off in the ‘insert-by-context’ minibuffer
zstyle ':completion:insert-by-context:*' insert-tab false

# Now, ‘insert-by-context’ works in three steps. First, it needs to establish
# which context a command line is in. That's done by a list of detection
# functions:
zstyle ':functions:insert-by-context:*' detectors ibc-atag-detect

# When the widget is entered, a list of generator functions is called, to
# generate a list of things, that the context sensitive completion in the
# minibuffer will offer.
zstyle ':functions:insert-by-context:atag' generators ibc-atag-tags

# Optionally, a competion callback can be defined to take over instead of the
# default completion (which just offers the generated data without any
# description).
zstyle ':functions:insert-by-context:atag' completer ibc-atag-complete

# Finally, a function may be defined that takes the string entered into the
# minibuffer and expands it into something else. This step is optional. The
# default behaviour is to insert the string from the minibuffer unchanged.
zstyle ':functions:insert-by-context:atag' expansion ibc-atag-expand

ibc-atag-detect() {
    # Detection: If there is a word "atag" in the command line buffer, assume
    # that a ‘atag’ call is being assembled. So, that's our job!
    local -a words
    words=( ${(z)BUFFER} )
    [[ -n ${(M)words:#atag} ]] || return 1
    REPLY=atag
    return 0
}

ibc-atag-tags() {
    # The strings, that should be offered are the keys to the ‘$a[]’ hash.
    # However, since we'll supply a custom completion, we don't need to
    # generate anything here (except for one entry, so that the framework
    # doesn't bail out).
    reply=( -dummy- )
}

typeset -ga IBC_ATAG_descriptions
for k v in ${(kv)ATAG__mapping}; do
    IBC_ATAG_descriptions+=( $v":$k tag" )
done

ibc-atag-complete() {
    # Offer all potential keys for the ‘$a[]’ hash with appropriate
    # descriptions, as generated into the ‘$IBC_ATAG_descriptions’ list.
    local expl
    _describe 'tag definitions' IBC_ATAG_descriptions -S ''
}

ibc-atag-expand() {
    # To be actually useful, when entering "tnp" for example, the framework
    # should actually insert "${a[tnp]}", because that will actually expand to
    # the right data.
    if (( NUMERIC  )); then
        REPLY='${atag['${(k)ATAG__mapping[(r)$1]}']}'
    else
        REPLY='${a['$1']}'
    fi
}

# load a number of smarter drop-in replacements for word actions
zstyle ':zle:*' word-style shell
for i in kill-word-match \
         forward-word-match \
         backward-word-match \
         transpose-words-match \
         delete-whole-word-match
do
    zrcautoload $i
    zle -N $i
done; unset i
zle -N kill-whole-word-match delete-whole-word-match

# string replacements in the current command buffer
zstyle ':zle:replace-pattern' edit-previous false
zrcautoload replace-string
zrcautoload replace-string-again
zle -N replace-pattern replace-string
zle -N replace-string-again

# Insert slashes in a "smart" way (well, not really all that smart...).
zrcautoload smart-slash && zle -N smart-slash

if zrcautoload jump-to-delim; then
    function jump-to-prev-/ () { jump-to-delim / 0 }
    zle -N jump-to-prev-/
fi

# backward clear from cursor to the next '/'
function backward-kill-to-slash() {
    (( MARK = CURSOR ))
    zle jump-to-prev-/ && zle kill-region
}
zle -N backward-kill-to-slash

# clear screen and reset prompt
function ft-clear-screen() {
    zle clear-screen
    zle reset-prompt
}
zle -N ft-clear-screen

# jump behind the Nth word on the cmdline.
function ft-jump-to-argument () {
    (( ${+NUMERIC} )) || NUMERIC=1
    zle .vi-first-non-blank
    zle forward-word-match
}
zle -N ft-jump-to-argument

if zrcautoload history-search-end ; then
    zle -N history-beginning-search-backward-end history-search-end
    zle -N history-beginning-search-forward-end  history-search-end
fi


### Custom Triggers for Completion ###########################################

# complete words from history
zle -C complete-history complete-word _generic
zstyle ':completion:complete-history:*' completer _history
# force filename completion
zle -C complete-files complete-word _generic
zstyle ':completion:complete-files:*' completer _files
# Complete words from tmux scrollback
ctxt=':completion:words-from-tmux'
for i in pane window session server; do
    zle -C words-from-tmux-$i complete-word _generic
    zstyle "$ctxt-$i:*" completer _words-from-tmux
    zle -C words-from-tmux-$i-anywhere complete-word _generic
    zstyle "$ctxt-$i-anywhere:*" completer _words-from-tmux
done
zstyle "$ctxt-*:*" ignore-line current
zstyle "$ctxt-*:*" menu yes select
zstyle "$ctxt-*-anywhere:*" matcher-list 'b:=*'
zstyle "$ctxt-*:*" capture-options -J -E - -S -
zstyle "$ctxt-*:*" pre-trim-pattern '[<"'\''`‘]##'
zstyle "$ctxt-*:*" post-trim-pattern '[>."'\''’]##'
zstyle "$ctxt-*:*" trim-pattern '[\\]'
unset i ctxt
# Tilde-prefix completion
zle -C ft-complete-tilde complete-word _generic
zstyle ':completion:ft-complete-tilde:*' completer \
       _tilde _expand _complete _ignored _approximate

function ft-complete-dots() {
    # Turns ".." into "cd ../../" puts the cursor behind the last `/'
    # and calls the completion system on that buffer.
    local REPLY
    turn_dots_into_cd "$BUFFER"
    BUFFER="$REPLY"/
    CURSOR="${#BUFFER}"
    zle complete-word
}
zle -N ft-complete-dots

function ft-complete-dirstack() {
    BUFFER="cd $BUFFER"
    CURSOR="${#BUFFER}"
    zle complete-word
}
zle -N ft-complete-dirstack

zstyle ':zle:ft-complete:tilde' widget ft-complete-tilde
zstyle ':zle:ft-complete:dots' widget ft-complete-dots
zstyle ':zle:ft-complete:dirstack' widget ft-complete-dirstack
zstyle ':zle:ft-complete:empty' action dot-slash-complete

function ft-complete() {
    setopt extendedglob localoptions
    local action context widget word
    local -a cmdline

    if [[ -z ${BUFFER} ]]; then
        context=empty
        zstyle -s ":zle:ft-complete:${context}" action action ||
            action=empty
        zstyle -s ":zle:ft-complete:${context}" widget widget ||
            widget=complete-word
        case ${action} in
        dot-slash-complete)
            if [[ $compcontext != -insert-by-context- ]]; then
                BUFFER='./'
                CURSOR=2
            fi
            zle ${widget} -w
            ;;
        empty)
            ;;
        *)
            zle ${widget} -w
            ;;
        esac

        return 0
    fi

    cmdline=( ${(z)BUFFER} )
    if (( ${#cmdline} == 1 )); then
        case ${cmdline[1]} in
        (.#)
            context=dots
            ;;
        ((-|+)(|<->))
        context=dirstack
        ;;
        (*)
            context=oneword
            ;;
        esac
        if [[ ${context} != oneword ]]; then
            zstyle -s ":zle:ft-complete:${context}" widget widget ||
                widget=complete-word
            zle ${widget} -w
            return 0
        fi
    fi

    word=${LBUFFER##* }
    if [[ ${word} == \~* ]] ; then
        context=tilde
        zstyle -s ":zle:ft-complete:${context}" widget widget ||
            widget=complete-word
        zle ${widget} -w
        return 0
    fi
    context=default
    zstyle -s ":zle:ft-complete:${context}" widget widget ||
        widget=complete-word
    zle ${widget} -w
    return 0
}
zle -N ft-complete


### vi Mode Integration ######################################################

# If we're using vi-mode, use either `ins' or `cmd' as the default mode.
zle_default_mode='ins'

# This makes sure the first prompt is drawn correctly.
if [[ ${zle_default_mode} == 'cmd' ]]; then
    psvar[1]='n'
else
    psvar[1]='i'
fi

# We're not in overwrite mode, when zsh starts.
ft_zle_state[overwrite]=no

# When this is hooked into `zle-keymap-select', keymap changes are correctly
# tracked in `psvar[1]', which may be used in PS1 as `%1v'.
function ft-psv1() {
    #RPS1="${ft_zle_state[overwrite]}, ${ft_zle_state[minibuffer]},"
    #RPS1="$RPS1 keymap: '$KEYMAP', widget: '$WIDGET'"
    if [[ ${CONTEXT} == 'vared' ]]; then
        psvar[1]='v'
    elif [[ ${ft_zle_state[minibuffer]} == yes ]]; then
        [[ ${psvar[1]} != *m ]] && psvar[1]="${psvar[1]}m"
    else
        case ${KEYMAP} in
        vicmd) psvar[1]='n';;
        viopp) psvar[1]='o';;
        visual) psvar[1]='v';;
        *)
            if [[ ${ft_zle_state[overwrite]} == yes ]]; then
                psvar[1]='r'
            else
                psvar[1]='i'
            fi
            ;;
        esac
    fi
    zle 'reset-prompt'
}

# This needs to be hooked into `zle-line-finish' to make sure the next
# newly drawn prompt has the correct mode display.
function ft-psv1-force() {
    if [[ ${zle_default_mode} == 'cmd' ]]; then
        psvar[1]='n'
    else
        psvar[1]='i'
    fi
}

# I created _functions[] arrays for `zle-line-init', `zle-line-finish' and
# `zle-keymap-select', too. Analogous to precmd_functions[] etc. The actual
# functions only cycle through these arrays and execute all existing
# functions in order. See below.
zle_keymap_functions=( ${zle_keymap_functions} ft-psv1 )
zle_finish_functions=( ${zle_finish_functions} ft-psv1-force )
if [[ ${zle_default_mode} == 'cmd' ]]; then
    zle_init_functions=( ${zle_init_functions} ft-vi-cmd )
fi
# Need to handle SIGINT, too (which is sent by ^C).
function TRAPINT() {
    ft_zle_state[minibuffer]=no
    ft-psv1-force
    zle reset-prompt 2>/dev/null
    return $(( 128 + $1 ))
}

function zle-line-init() {
    local w
    for w in "${zle_init_functions[@]}"; do
        (( ${+functions[$w]} )) && "$w"
    done
}

function zle-line-finish() {
    local w
    for w in "${zle_finish_functions[@]}"; do
        (( ${+functions[$w]} )) && "$w"
    done
}

function zle-keymap-select() {
    #setopt localoptions xtrace
    local w
    for w in "${zle_keymap_functions[@]}"; do
        (( ${+functions[$w]} )) && "$w"
    done
}

zle -N zle-line-init
zle -N zle-line-finish
zle -N zle-keymap-select

bindkey -v

# Since I killed `^d' for quickly escaping from a shell, this enables me
# to do `:q' in vi-cmd mode.
function ft-zshexit {
    [[ -o hist_ignore_space ]] && BUFFER=' '
    BUFFER="${BUFFER}exit"
    zle .accept-line
}
zle -N q ft-zshexit

# First the ones that change the input method directly; namely cmd mode,
# insert mode and replace mode.
function ft-vi-replace() {
    ft_zle_state[overwrite]=yes
    zle vi-replace
    ft-psv1
}

function ft-vi-insert() {
    ft_zle_state[overwrite]=no
    zle vi-insert
}

# Since I want to bind `vi-cmd-mode' to Ctrl-D (which is what I'm doing in
# vim and emacs-viper, too) I need to wrap this widget into a user-widget,
# because only those have an effect with empty command buffers and bindings
# to the key, which sends `EOF'. This also needs the ignore_eof option set.
function ft-vi-cmd() {
    ft_zle_state[overwrite]=no
    zle vi-cmd-mode
}

function ft-vi-cmd-cmd() {
    zle -M 'Use `:q<RET>'\'' to exit the shell.'
}

# ...and now the widgets that open minibuffers...
# Oh, yeah. You cannot wrap `execute-named-cmd', so no minibuffer-signaling
# for that. See <http://www.zsh.org/mla/workers/2005/msg00384.html>.
function ft-markminibuf() {
    ft_zle_state[minibuffer]=yes
    ft-psv1
    zle "$1"
    ft_zle_state[minibuffer]=no
    ft-psv1
}

if (( ${+widgets[.history-incremental-pattern-search-backward]} )); then
    function history-incremental-pattern-search-backward() {
        ft-markminibuf .history-incremental-pattern-search-backward
    }
else
    function history-incremental-search-backward() {
        ft-markminibuf .history-incremental-search-backward
    }
fi

if (( ${+widgets[.history-incremental-pattern-search-forward]} )); then
    function history-incremental-pattern-search-forward() {
        ft-markminibuf .history-incremental-pattern-search-forward
    }
else
    function history-incremental-search-forward() {
        ft-markminibuf .history-incremental-search-forward
    }
fi

function ft-vi-search-back() {
    ft-markminibuf vi-history-search-backward
}

function ft-vi-search-fwd() {
    ft-markminibuf vi-history-search-forward
}

function ft-replace-pattern() {
    ft-markminibuf replace-pattern
}

# register the created widgets
for w in  ft-replace-pattern \
            ft-vi-{cmd,cmd-cmd,replace,insert,search-back,search-fwd}
do
    zle -N "$w"
done; unset w

function ft-dark-terminal () {
    prompt_ft_choose d
    prompt_ft_precmd
    FT_HIGHLIGHT_BG=dark
    zle reset-prompt
}

function ft-light-terminal () {
    prompt_ft_choose l
    prompt_ft_precmd
    FT_HIGHLIGHT_BG=light
    zle reset-prompt
}

zle -N dark ft-dark-terminal
zle -N light ft-light-terminal

# Inserting timestamps for esport recordings
function ft-twitch-timestamp() {
    RBUFFER='?t=0m32s'"$RBUFFER"
    zle forward-char -n 3
}
zle -N ft-twitch-timestamp


### Keybindings ##############################################################

# I setopt ignore_eof in options.z, so the following can work.
bind2maps viins -- Ctrl-d ft-vi-cmd
bind2maps vicmd -- Ctrl-d ft-vi-cmd-cmd

# Remove the escape key binding.
bindkey -r '^['

# The bindings without -M <mapname> go to the `main' keymap. That's
# either the main emacs map or viins. So this really only needs to
# bother with binding things in `vicmd' specially.

if (( ${+widgets[.history-incremental-pattern-search-backward]} )); then
    bind2maps vicmd viins emacs \
              -- Ctrl-r history-incremental-pattern-search-backward
else
    bind2maps vicmd viins emacs -- Ctrl-r history-incremental-search-backward
fi
if (( ${+widgets[.history-incremental-pattern-search-forward]} )); then
    bind2maps vicmd viins emacs \
              -- Ctrl-s history-incremental-pattern-search-forward
else
    bind2maps vicmd viins emacs \
              -- Ctrl-s history-incremental-search-forward
fi

bind2maps vicmd             -- ":" execute-named-cmd
bind2maps vicmd viins emacs -- "Alt-x" execute-named-cmd
bind2maps vicmd             -- "/" ft-vi-search-fwd
bind2maps vicmd             -- "?" ft-vi-search-back
bind2maps vicmd             -- "i" ft-vi-insert
bind2maps vicmd             -- "R" ft-vi-replace
bind2maps vicmd             -- "u" undo
bind2maps       viins emacs -- "^u" undo
bind2maps vicmd viins emacs -- "Alt-h" run-help
bind2maps       viins emacs -- "Tab" ft-complete
bind2maps       viins emacs -- "Home" beginning-of-line
bind2maps       viins emacs -- "End" end-of-line
bind2maps       viins emacs -- "Insert" overwrite-mode
bind2maps       viins emacs -- "Delete" delete-char
bind2maps       viins emacs -- "Up" up-line-or-history
bind2maps       viins emacs -- "Down" down-line-or-history
bind2maps       viins emacs -- "Left" backward-char
bind2maps       viins emacs -- "Right" forward-char
bind2maps       viins emacs -- "Ctrl-x p" history-search-backward
bind2maps       viins emacs -- "Ctrl-x P" history-search-forward
bind2maps       viins emacs -- "Ctrl-x k" backward-kill-line
bind2maps       viins emacs -- "Ctrl-l" ft-clear-screen
bind2maps       viins emacs -- "Alt-y" _most_recent_file
bind2maps       viins emacs -- "Alt-Enter" self-insert-unmeta
bind2maps       viins emacs -- "Ctrl-p" up-line-or-history
bind2maps       viins emacs -- "Ctrl-n" down-line-or-history
bind2maps       viins emacs -- "Ctrl-w" backward-kill-word
bind2maps       viins emacs -- "Backspace" backward-delete-char
bind2maps       viins emacs -- -s "^?" backward-delete-char
for (( i = 0; i < 10; ++i )); do
bind2maps vicmd viins emacs -- "Alt-$i" digit-argument
done; unset i
bind2maps       viins emacs -- "Alt-D" zap-to-char
bind2maps       viins emacs -- "Alt-F" delete-to-char
bind2maps       viins emacs -- "Alt-b" backward-word-match
bind2maps       viins emacs -- "Alt-d" kill-word-match
bind2maps       viins emacs -- "Alt-e" edit-command-line
bind2maps       viins emacs -- "Alt-f" forward-word-match
bind2maps       viins emacs -- "Alt-t" tranpose-words-match
bind2maps       viins emacs -- "Alt-h" run-help
bind2maps vicmd viins emacs -- "Alt-k" up-history
bind2maps vicmd viins emacs -- "Alt-j" down-history
bind2maps vicmd viins emacs -- "Alt-n" ft-jump-to-argument
bind2maps       viins emacs -- "Alt-." smart-slash
bind2maps       viins emacs -- "Alt-w" backward-kill-to-slash
bind2maps vicmd viins emacs -- "Alt-q" push-input
bind2maps vicmd viins emacs -- "Ctrl-x ," ft-replace-pattern
bind2maps vicmd viins emacs -- "Ctrl-x ." replace-string-again
bind2maps       viins emacs -- "Ctrl-x d" kill-whole-word-match
bind2maps       viins emacs -- "Ctrl-x f" complete-files
bind2maps vicmd viins emacs -- 'Alt-s p' words-from-tmux-pane-anywhere
bind2maps vicmd viins emacs -- 'Alt-s w' words-from-tmux-window-anywhere
bind2maps vicmd viins emacs -- 'Alt-s s' words-from-tmux-session-anywhere
bind2maps vicmd viins emacs -- 'Alt-s e' words-from-tmux-server-anywhere
bind2maps vicmd viins emacs -- 'Alt-s ap' words-from-tmux-pane
bind2maps vicmd viins emacs -- 'Alt-s aw' words-from-tmux-window
bind2maps vicmd viins emacs -- 'Alt-s as' words-from-tmux-session
bind2maps vicmd viins emacs -- 'Alt-s ae' words-from-tmux-server
bind2maps vicmd viins emacs -- "Ctrl-x h" complete-history
bind2maps vicmd viins emacs -- "Ctrl-x H" _complete_help
bind2maps vicmd viins emacs -- "Ctrl-x D" _complete_debug
bind2maps vicmd viins emacs -- "Ctrl-o d" dark
bind2maps vicmd viins emacs -- "Ctrl-o l" light
bind2maps vicmd viins emacs -- "PageUp" history-beginning-search-backward-end
bind2maps vicmd viins emacs -- "PageDown" history-beginning-search-forward-end
bind2maps vicmd viins emacs -- "Ctrl-x i" insert-by-context
bind2maps menuselect -- "Alt-Enter" accept-and-menu-complete
bind2maps menuselect -- "Alt-n" accept-and-infer-next-history
bind2maps menuselect -- "Alt-m" vi-insert
bind2maps menuselect -- "Ctrl-s" history-incremental-search-forward
bind2maps menuselect -- "Ctrl-r" history-incremental-search-backward
bind2maps menuselect -- "Alt-h" vi-backward-char
bind2maps menuselect -- "Alt-j" vi-down-line-or-history
bind2maps menuselect -- "Alt-k" vi-up-line-or-history
bind2maps menuselect -- "Alt-l" vi-forward-char
bind2maps menuselect -- "^g" send-break


### Guix integration #########################################################

if [[ -n "$GUIX_ENVIRONMENT" ]]; then
    printf 'Running in guix environment: %s\n' "$GUIX_ENVIRONMENT"
else
    typeset -gx GUIX_LOCPATH="$HOME/.guix-profile/lib/locale"
    typeset -gx INFOPATH MANPATH
    typeset -gaU infopath manpath
    typeset -T INFOPATH infopath ":"
    typeset -T MANPATH manpath ":"

    if [[ -x ${commands[manpath]} ]]; then
        MANPATH="$(MANPATH= manpath)"
    fi

    manpath+=( /opt/emacs/share/man
               /usr/local/share/man
               /usr/share/man )

    infopath=( /opt/emacs/share/info
               /usr/local/share/info
               /usr/share/info )

    typeset -a __guix__=( "$HOME/.guix-profile"
                          "$HOME/.config/guix/current" )

    function guix_source () {
        local file="$1"
        test -e "$file" && emulate sh -c "source $file"
    }

    for GUIX_PROFILE in "${__guix__[@]}"; do
        guix_source "$GUIX_PROFILE/etc/profile"
    done

    unset __guix__ GUIX_PROFILE
    unfunction guix_source
    typeset -gx GUIX_PROFILE="$HOME/.guix-profile"
fi

### Late setup ###############################################################

umask 0077
ulimit -c unlimited

[[ -n $TMUX ]] && cdm


### Post setup optional config file and profiling ############################

source_if_exists zshrc.post

if [[ $ZSHRC_PROFILE == 1 ]]; then
    zprof | less
fi
# Make sure the first prompt doesn't signal an error:
true
