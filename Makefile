HARNESS = prove --merge --color
TA_TESTS = ./tests/jump-to-delim.t

zwc-clean:
	rm -f ~/.zshrc.zwc ~/.zshrc.real.zwc
	find ~/.zfunctions -type f -name "*.zwc" -exec rm -f '{}' '+'

test:
	$(HARNESS) --verbose $(TA_TESTS)
	zsh -f tests/vcsup.t

.PHONY: test zwc-clean
