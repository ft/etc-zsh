# Display the last few line of ‘dmesg’ output. “few” is just a bit less lines
# than there are lines in the running terminal, so the function's output will
# fit into the terminal, unless the output is very long with lots of wrapping
# lines.
#
# If the number of lines of a terminal could not be determined, 24 is used as
# a default. The ratio to determine how many lines to actually print defaults
# to 85%.
#
# The following styles may be used to customise the behaviour of the function:
#
#   - default-lines: An integer that will be used instead of 24 as the default
#                    number of lines in a terminal, that didn't allow the
#                    number of lines to be determined.
#
#   - ratio: The ratio of lines to be printed. The default is "0.85".
#
# All configuration is done in the ':fnc:dm:options' context.

emulate -L zsh
setopt extended_glob

local context=':fnc:dm:options'
local default_lines
local -F ratio

zstyle -s $context default-lines default_lines || default_lines=24

local -i lines=${LINES:-$default_lines}

if (( ARGC == 1 )); then
    if [[ $1 != [0-9]## ]]; then
        printf 'dm: Argument has to be a positive integer!\n'
        return 1
    fi
    lines=$1
elif (( ARGC > 1 )); then
    printf 'usage: dm [number-of-lines]\n'
    return 1
else
    zstyle -s $context ratio ratio || ratio=0.85
    lines=$(( lines * ratio ))
fi

dmesg | tail -n$lines
